/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.utils;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Locale;

import javax.imageio.spi.ImageInputStreamSpi;
import javax.imageio.stream.ImageInputStream;

public class ByteBufferImageInputStreamSpi extends ImageInputStreamSpi
{
	
	public ByteBufferImageInputStreamSpi()
	{
		super("Felix Treede", "1.0", ByteBuffer.class);
	}
	
	@Override
	public ImageInputStream createInputStreamInstance(Object input, boolean useCache, File cacheDir) throws IOException
	{
		return createInputStreamInstance(input);
	}
	
	@Override
	public ImageInputStream createInputStreamInstance(Object input) throws IOException
	{
		ByteBuffer buffer;
		if (input instanceof ByteBuffer) {
			buffer = (ByteBuffer) input;
		} else if (input instanceof byte[]) {
			byte[] b = (byte[]) input;
			buffer = ByteBuffer.wrap(b);
		} else
			throw new IllegalArgumentException();
		return new ByteBufferImageInputStream(buffer);
	}
	
	@Override
	public String getDescription(Locale locale)
	{
		return "Service provider that instantiates a FileImageInputStream from a ByteBuffer";
	}
	
}
