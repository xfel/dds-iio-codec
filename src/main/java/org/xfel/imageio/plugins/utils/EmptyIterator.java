/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.utils;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class EmptyIterator<E> implements ListIterator<E> {

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public E next() {
		throw new NoSuchElementException("Iterator is empty");
	}

	@Override
	public void remove() {
		throw new IllegalStateException("Next method has not yet been called");
	}

	@Override
	public boolean hasPrevious()
	{
		return false;
	}

	@Override
	public E previous()
	{
		throw new NoSuchElementException("Iterator is empty");
	}

	@Override
	public int nextIndex()
	{
		return 0;
	}

	@Override
	public int previousIndex()
	{
		return -1;
	}

	@Override
	public void set(E e)
	{
		throw new IllegalStateException("Next method has not yet been called");
	}

	@Override
	public void add(E e)
	{
		throw new IllegalStateException("Next method has not yet been called");
	}

}
