/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;

import javax.imageio.stream.ImageInputStreamImpl;

public class ChannelImageInputStream extends ImageInputStreamImpl
{

	protected ByteBuffer buffer;

	protected SeekableByteChannel channel;

	public ChannelImageInputStream(SeekableByteChannel channel)
	{
		super();
		this.channel = channel;
		buffer = ByteBuffer.allocateDirect(8192);
	}

	@Override
	public void close() throws IOException
	{
		super.close();
		channel.close();
	}

	@Override
	public long length()
	{
		try {
			checkClosed();
			return channel.size();
		} catch (IOException e) {
			return -1L;
		}
	}

	@Override
	public void seek(long pos) throws IOException
	{
		long currentPos = getStreamPosition();
		super.seek(pos);

		long diff = pos - currentPos;
		if (diff > -buffer.position() && diff < buffer.remaining()) {
			buffer.position((int) (buffer.position() + diff));
		} else {
			channel.position(pos);
			fillBuffer();
		}
	}

	private void fillBuffer() throws IOException
	{
		buffer.clear();
		channel.read(buffer);
	}

	@Override
	public int read() throws IOException
	{
		checkClosed();
		bitOffset = 0;
		if (channel.position() == channel.size()) {
			return -1;
		}
		if (buffer.remaining() <1) {
			fillBuffer();
		}
		return buffer.get();
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException
	{
		checkClosed();
		bitOffset = 0;
		if (buffer.remaining() < 1) {
			fillBuffer();
		}
		int read = 0;
		read += Math.min(len, buffer.remaining());
		buffer.get(b, off + read, Math.min(len, buffer.remaining()));
		while (len > 0) {
			if (channel.position() == channel.size()) {
				break;
			}
			fillBuffer();
			read += Math.min(len, buffer.remaining());
			buffer.get(b, off + read, Math.min(len, buffer.remaining()));
		}
		return read;
	}
	

}
