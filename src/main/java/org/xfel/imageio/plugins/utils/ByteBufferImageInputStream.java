/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.utils;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import javax.imageio.stream.ImageInputStreamImpl;

public class ByteBufferImageInputStream extends ImageInputStreamImpl
{
	
	protected ByteBuffer buffer;
	
	public ByteBufferImageInputStream(ByteBuffer buffer)
	{
		this.buffer = buffer;
	}

	@Override
	public void setByteOrder(ByteOrder byteOrder)
	{
		buffer.order(byteOrder);
	}
	
	@Override
	public ByteOrder getByteOrder()
	{
		return buffer.order();
	}
	
	@Override
	public int read() throws IOException
	{
		checkClosed();
		return buffer.get();
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException
	{
		checkClosed();
		if (len > buffer.remaining()) {
			len = buffer.remaining();
		}
		buffer.get(b, off, len);
		return len;
	}
	
	@Override
	public byte readByte() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 1)
			throw new EOFException();
		return buffer.get();
	}
	
	@Override
	public int readUnsignedByte() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 1)
			throw new EOFException();
		return buffer.get()&0xFF;
	}
	
	@Override
	public short readShort() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 2)
			throw new EOFException();
		return buffer.getShort();
	}
	
	@Override
	public int readUnsignedShort() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 2)
			throw new EOFException();
		return buffer.getShort()&0xFFFF;
	}
	
	@Override
	public char readChar() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 2)
			throw new EOFException();
		return buffer.getChar();
	}
	
	@Override
	public int readInt() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 4)
			throw new EOFException();
		return buffer.getInt();
	}
	
	@Override
	public long readUnsignedInt() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 4)
			throw new EOFException();
		return buffer.getInt()&0xFFFFFFFF;
	}
	
	@Override
	public long readLong() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 8)
			throw new EOFException();
		return buffer.getLong();
	}
	
	@Override
	public float readFloat() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 4)
			throw new EOFException();
		return buffer.getFloat();
	}
	
	@Override
	public double readDouble() throws IOException
	{
		checkClosed();
		if (buffer.remaining() < 8)
			throw new EOFException();
		return buffer.getDouble();
	}
	
	@Override
	public String readLine() throws IOException
	{
		StringBuilder sb = new StringBuilder();
		boolean eol = false;
		
		while (!eol && (buffer.remaining() >= 2)) {
			char c = buffer.getChar();
			switch (c) {
			case '\n':
				eol = true;
				break;
			case '\r':
				eol = true;
				if (buffer.getChar(buffer.position()) == '\n') {
					buffer.getChar();
				}
				break;
			default:
				sb.append(c);
			}
		}
		if ((buffer.remaining() < 2) && (sb.length() == 0))
			return null;
		return sb.toString();
	}
	
	@Override
	public String readUTF() throws IOException
	{
		checkClosed();
		return DataInputStream.readUTF(this);
	}
	
	@Override
	public void readFully(byte[] b, int off, int len) throws IOException
	{
		checkClosed();
		
		if (len > buffer.remaining())
			throw new EOFException();
		
		buffer.get(b, off, len);
	}
	
	@Override
	public void readFully(short[] s, int off, int len) throws IOException
	{
		checkClosed();
		
		ShortBuffer buffer = this.buffer.asShortBuffer();
		if (len > buffer.remaining())
			throw new EOFException();
		
		buffer.get(s, off, len);
	}
	
	@Override
	public void readFully(char[] c, int off, int len) throws IOException
	{
		checkClosed();
		
		CharBuffer buffer = this.buffer.asCharBuffer();
		if (len > buffer.remaining())
			throw new EOFException();
		
		buffer.get(c, off, len);
	}
	
	@Override
	public void readFully(int[] i, int off, int len) throws IOException
	{
		checkClosed();
		
		IntBuffer buffer = this.buffer.asIntBuffer();
		if (len > buffer.remaining())
			throw new EOFException();
		
		buffer.get(i, off, len);
	}
	
	@Override
	public void readFully(long[] l, int off, int len) throws IOException
	{
		checkClosed();
		
		LongBuffer buffer = this.buffer.asLongBuffer();
		if (len > buffer.remaining())
			throw new EOFException();
		
		buffer.get(l, off, len);
	}
	
	@Override
	public void readFully(float[] f, int off, int len) throws IOException
	{
		checkClosed();
		
		FloatBuffer buffer = this.buffer.asFloatBuffer();
		if (len > buffer.remaining())
			throw new EOFException();
		
		buffer.get(f, off, len);
	}
	
	@Override
	public void readFully(double[] d, int off, int len) throws IOException
	{
		checkClosed();
		
		DoubleBuffer buffer = this.buffer.asDoubleBuffer();
		if (len > buffer.remaining())
			throw new EOFException();
		
		buffer.get(d, off, len);
	}
	
	@Override
	public long getStreamPosition() throws IOException
	{
		return buffer.position();
	}
	
	@Override
	public long length()
	{
		return buffer.capacity();
	}
	
	@Override
	public void seek(long pos) throws IOException
	{
		checkClosed();
		
		// This test also covers pos < 0
		if (pos < flushedPos)
			throw new IndexOutOfBoundsException("pos < flushedPos!");
		
		this.streamPos = pos;
		this.bitOffset = 0;
		buffer.position((int) pos);
	}
	
	@Override
	public void flushBefore(long pos) throws IOException
	{
		checkClosed();
	}
	
	@Override
	public void flush() throws IOException
	{
		checkClosed();
	}
	
	@Override
	public long getFlushedPosition()
	{
		return 0;
	}
	
	@Override
	public void close() throws IOException
	{
		super.close();
		buffer = null;
	}
	
}
