/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;

import javax.imageio.stream.ImageOutputStreamImpl;

public class ChannelImageOutputStream extends ImageOutputStreamImpl
{

	protected ByteBuffer buffer;

	protected SeekableByteChannel channel;

	public ChannelImageOutputStream(SeekableByteChannel channel)
	{
		super();
		this.channel = channel;
		buffer = ByteBuffer.allocateDirect(8192);
		
	}

	@Override
	public void close() throws IOException
	{
		super.close();
		channel.close();
	}

	@Override
	public long length()
	{
		try {
			checkClosed();
			return channel.size();
		} catch (IOException e) {
			return -1L;
		}
	}

	@Override
	public void seek(long pos) throws IOException
	{
		long currentPos = getStreamPosition();
		super.seek(pos);

		long diff = pos - currentPos;
		if (diff > -buffer.position() && diff < buffer.remaining()) {
			buffer.position((int) (buffer.position() + diff));
		} else {
			channel.position(pos);
			fillBuffer();
		}
	}

	private void fillBuffer() throws IOException
	{
		buffer.clear();
		channel.read(buffer);
	}

	private void flushBuffer() throws IOException
	{
		channel.position(channel.position()-buffer.capacity());
		channel.write(buffer);
		fillBuffer();
	}

	@Override
	public int read() throws IOException
	{
		checkClosed();
		bitOffset = 0;
		if (channel.position() == channel.size()) {
			return -1;
		}
		if (buffer.remaining() == 0) {
			fillBuffer();
		}
		return buffer.get();
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException
	{
		checkClosed();
		bitOffset = 0;
		if (buffer.remaining() < 1) {
			fillBuffer();
		}
		int read = 0;
		int min = Math.min(len, buffer.remaining());
		read += min;
		buffer.get(b, off + read, min);
		len -= min;
		while (len > 0) {
			if (channel.position() == channel.size()) {
				break;
			}
			fillBuffer();
			
			min = Math.min(len, buffer.remaining());
			read += min;
			buffer.get(b, off + read, min);
			len -= min;
		}
		return read;
	}

	@Override
	public void write(int b) throws IOException
	{
		flushBits();
		if (buffer.remaining() < 1) {
			flushBuffer();
		}
		buffer.put((byte) (b & 0xFF));
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException
	{
		flushBits();
		if (buffer.remaining() < 1) {
			flushBuffer();
		}
		int written = 0;
		int min = Math.min(len, buffer.remaining());
		written += min;
		buffer.put(b, off + written, min);
		len -= min;
		
		while(len>0){
			flushBuffer();
			min = Math.min(len, buffer.remaining());
			written += min;
			buffer.put(b, off + written, min);
			len -= min;
		}
	}

}
