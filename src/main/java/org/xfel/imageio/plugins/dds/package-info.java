/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * Image Index = Cubemap face
 * 
 * Thumbnails = mipmaps
 * 
 * If subsampling is used while reading, the reader will try to find a smaller MipMap which fits better.
 * 
 * 
 * @author Felix Treede
 * 
 */
package org.xfel.imageio.plugins.dds;

