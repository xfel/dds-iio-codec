/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DirectColorModel;
import java.awt.image.SampleModel;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.imageio.ImageTypeSpecifier;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

public class DDSFileHeader implements DDSConstants
{
	
	static int makeFourCC(char ch0, char ch1, char ch2, char ch3)
	{
		return ((byte) (ch0) | ((byte) (ch1) << 8) |
				((byte) (ch2) << 16) | ((byte) (ch3) << 24));
	}
	
	/**
	 * <p>
	 * Flags to indicate which members contain valid data.
	 * </p>
	 * <table>
	 * <tbody>
	 * <tr>
	 * <th>Flag</th>
	 * <th>Description</th>
	 * <th>Value</th>
	 * </tr>
	 * <tr>
	 * <td>DDSD_CAPS</td>
	 * <td>Required in every .dds file.</td>
	 * <td>0x1</td>
	 * </tr>
	 * <tr>
	 * <td>DDSD_HEIGHT</td>
	 * <td>Required in every .dds file.</td>
	 * <td>0x2</td>
	 * </tr>
	 * <tr>
	 * <td>DDSD_WIDTH</td>
	 * <td>Required in every .dds file.</td>
	 * <td>0x4</td>
	 * </tr>
	 * <tr>
	 * <td>DDSD_PITCH</td>
	 * <td>Required when pitch is provided for an uncompressed texture.</td>
	 * <td>0x8</td>
	 * </tr>
	 * <tr>
	 * <td>DDSD_PIXELFORMAT</td>
	 * <td>Required in every .dds file.</td>
	 * <td>0x1000</td>
	 * </tr>
	 * <tr>
	 * <td>DDSD_MIPMAPCOUNT</td>
	 * <td>Required in a mipmapped texture.</td>
	 * <td>0x20000</td>
	 * </tr>
	 * <tr>
	 * <td>DDSD_LINEARSIZE</td>
	 * <td>Required when pitch is provided for a compressed texture.</td>
	 * <td>0x80000</td>
	 * </tr>
	 * <tr>
	 * <td>DDSD_DEPTH</td>
	 * <td>Required in a depth texture.</td>
	 * <td>0x800000</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 * <p>
	 * &nbsp;
	 * </p>
	 * <p class="note">
	 * <strong>Note</strong>&nbsp;&nbsp;When you write .dds files, you should set the DDSD_CAPS and DDSD_PIXELFORMAT flags, and for mipmapped textures you should also set the
	 * DDSD_MIPMAPCOUNT flag. However, when you read a .dds file, you should not rely on the DDSD_CAPS, DDSD_PIXELFORMAT, and DDSD_MIPMAPCOUNT flags being set because some writers
	 * of such a file might not set these flags.
	 * </p>
	 * <p>
	 * The DDS_HEADER_FLAGS_TEXTURE flag, which is defined in Dds.h, is a bitwise-OR combination of the DDSD_CAPS, DDSD_HEIGHT, DDSD_WIDTH, and DDSD_PIXELFORMAT flags.
	 * </p>
	 * <p>
	 * The DDS_HEADER_FLAGS_MIPMAP flag, which is defined in Dds.h, is equal to the DDSD_MIPMAPCOUNT flag.
	 * </p>
	 * <p>
	 * The DDS_HEADER_FLAGS_VOLUME flag, which is defined in Dds.h, is equal to the DDSD_DEPTH flag.
	 * </p>
	 * <p>
	 * The DDS_HEADER_FLAGS_PITCH flag, which is defined in Dds.h, is equal to the DDSD_PITCH flag.
	 * </p>
	 * <p>
	 * The DDS_HEADER_FLAGS_LINEARSIZE flag, which is defined in Dds.h, is equal to the DDSD_LINEARSIZE flag.
	 * </p>
	 */
	public int headerFlags;
	
	/**
	 * Surface height (in pixels).
	 */
	public int height;
	
	/**
	 * Surface width (in pixels).
	 */
	public int width;
	
	/**
	 * The number of bytes per scan line in an uncompressed texture; the total
	 * number of bytes in the top level texture for a compressed texture. The
	 * pitch must be DWORD aligned.
	 */
	public int pitchOrLinearSize;
	/**
	 * Depth of a volume texture (in pixels), otherwise unused.
	 */
	public int depth;
	/**
	 * Number of mipmap levels, otherwise unused.
	 */
	public int mipMapCount;
	
	/**
	 * <p>
	 * Values which indicate what type of data is in the surface.
	 * </p>
	 * <table>
	 * <tbody>
	 * <tr>
	 * <th>Flag</th>
	 * <th>Description</th>
	 * <th>Value</th>
	 * </tr>
	 * <tr>
	 * <td>DDPF_ALPHAPIXELS</td>
	 * <td>Texture contains alpha data; <strong>dwRGBAlphaBitMask</strong> contains valid data.</td>
	 * <td>0x1</td>
	 * </tr>
	 * <tr>
	 * <td>DDPF_ALPHA</td>
	 * <td>Used in some older DDS files for alpha channel only uncompressed data (dwRGBBitCount contains the alpha channel bitcount; dwABitMask contains valid data)</td>
	 * <td>0x2</td>
	 * </tr>
	 * <tr>
	 * <td>DDPF_FOURCC</td>
	 * <td>Texture contains compressed RGB data; <strong>dwFourCC</strong> contains valid data.</td>
	 * <td>0x4</td>
	 * </tr>
	 * <tr>
	 * <td>DDPF_RGB</td>
	 * <td>Texture contains uncompressed RGB data; <strong>dwRGBBitCount</strong> and the RGB masks (<strong>dwRBitMask</strong>, <strong>dwRBitMask</strong>,
	 * <strong>dwRBitMask</strong>) contain valid data.</td>
	 * <td>0x40</td>
	 * </tr>
	 * <tr>
	 * <td>DDPF_YUV</td>
	 * <td>Used in some older DDS files for YUV uncompressed data (dwRGBBitCount contains the YUV bit count; dwRBitMask contains the Y mask, dwGBitMask contains the U mask,
	 * dwBBitMask contains the V mask)</td>
	 * <td>0x200</td>
	 * </tr>
	 * <tr>
	 * <td>DDPF_LUMINANCE</td>
	 * <td>Used in some older DDS files for single channel color uncompressed data (dwRGBBitCount contains the luminance channel bit count; dwRBitMask contains the channel mask).
	 * Can be combined with DDPF_ALPHAPIXELS for a two channel DDS file.</td>
	 * <td>0x20000</td>
	 */
	public int pfFlags;
	
	/**
	 * Four-character codes for specifying compressed or custom formats.
	 * Possible values include: DXT1, DXT2, DXT3, DXT4, or DXT5. A FourCC of
	 * DX10 indicates the prescense of the DDS_HEADER_DXT10 extended header, and
	 * the dxgiFormat member of that structure indicates the true format. When
	 * using a four-character code, dwFlags must include DDPF_FOURCC.
	 */
	public int pfFourCC;
	
	/**
	 * Number of bits in an RGB (possibly including alpha) format. Valid when
	 * dwFlags includes DDPF_RGB, DDPF_LUMINANCE, or DDPF_YUV.
	 */
	public int pfRGBBitCount;
	/**
	 * Red (or lumiannce or Y) mask for reading color data. F or instance, given
	 * the A8R8G8B8 format, the red mask would be 0x00ff0000.
	 */
	public int pfRBitMask;
	/**
	 * Green (or U) mask for reading color data. For instance, given the
	 * A8R8G8B8 format, the green mask would be 0x0000ff00.
	 */
	public int pfGBitMask;
	/**
	 * Blue (or V) mask for reading color data. For instance, given the A8R8G8B8
	 * format, the blue mask would be 0x000000ff.
	 */
	public int pfBBitMask;
	/**
	 * Alpha mask for reading alpha data. dwFlags must include DDPF_ALPHAPIXELS
	 * or DDPF_ALPHA. For instance, given the A8R8G8B8 format, the alpha mask
	 * would be 0xff000000.
	 */
	public int pfABitMask;
	
	/**
	 * <p>
	 * Specifies the complexity of the surfaces stored.
	 * </p>
	 * <table>
	 * <tbody>
	 * <tr>
	 * <th>Flag</th>
	 * <th>Description</th>
	 * <th>Value</th>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS_COMPLEX</td>
	 * <td>Optional; must be used on any file that contains more than one surface (a mipmap, a cubic environment map, or mipmapped volume texture).</td>
	 * <td>0x8</td>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS_MIPMAP</td>
	 * <td>Optional; should be used for a mipmap.</td>
	 * <td>0x400000</td>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS_TEXTURE</td>
	 * <td>Required</td>
	 * <td>0x1000</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 * <p>
	 * &nbsp;
	 * </p>
	 * <p class="note">
	 * <strong>Note</strong>&nbsp;&nbsp;When you write .dds files, you should set the DDSCAPS_TEXTURE flag, and for multiple surfaces you should also set the DDSCAPS_COMPLEX flag.
	 * However, when you read a .dds file, you should not rely on the DDSCAPS_TEXTURE and DDSCAPS_COMPLEX flags being set because some writers of such a file might not set these
	 * flags.
	 * </p>
	 * <p>
	 * The DDS_SURFACE_FLAGS_MIPMAP flag, which is defined in Dds.h, is a bitwise-OR combination of the DDSCAPS_COMPLEX and DDSCAPS_MIPMAP flags.
	 * </p>
	 * <p>
	 * The DDS_SURFACE_FLAGS_TEXTURE flag, which is defined in Dds.h, is equal to the DDSCAPS_TEXTURE flag.
	 * </p>
	 * <p>
	 * The DDS_SURFACE_FLAGS_CUBEMAP flag, which is defined in Dds.h, is equal to the DDSCAPS_COMPLEX flag.
	 * </p>
	 */
	public int surfaceFlags;
	
	/**
	 * <p>
	 * Additional detail about the surfaces stored.
	 * </p>
	 * <table>
	 * <tbody>
	 * <tr>
	 * <th>Flag</th>
	 * <th>Description</th>
	 * <th>Value</th>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS2_CUBEMAP</td>
	 * <td>Required for a cube map.</td>
	 * <td>0x200</td>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS2_CUBEMAP_POSITIVEX</td>
	 * <td>Required when these surfaces are stored in a cube map.</td>
	 * <td>0x400</td>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS2_CUBEMAP_NEGATIVEX</td>
	 * <td>Required when these surfaces are stored in a cube map.</td>
	 * <td>0x800</td>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS2_CUBEMAP_POSITIVEY</td>
	 * <td>Required when these surfaces are stored in a cube map.</td>
	 * <td>0x1000</td>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS2_CUBEMAP_NEGATIVEY</td>
	 * <td>Required when these surfaces are stored in a cube map.</td>
	 * <td>0x2000</td>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS2_CUBEMAP_POSITIVEZ</td>
	 * <td>Required when these surfaces are stored in a cube map.</td>
	 * <td>0x4000</td>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS2_CUBEMAP_NEGATIVEZ</td>
	 * <td>Required when these surfaces are stored in a cube map.</td>
	 * <td>0x8000</td>
	 * </tr>
	 * <tr>
	 * <td>DDSCAPS2_VOLUME</td>
	 * <td>Required for a volume texture.</td>
	 * <td>0x200000</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 * <p>
	 * &nbsp;
	 * </p>
	 * <p>
	 * The DDS_CUBEMAP_POSITIVEX flag, which is defined in Dds.h, is a bitwise-OR combination of the DDSCAPS2_CUBEMAP and DDSCAPS2_CUBEMAP_POSITIVEX flags.
	 * </p>
	 * <p>
	 * The DDS_CUBEMAP_NEGATIVEX flag, which is defined in Dds.h, is a bitwise-OR combination of the DDSCAPS2_CUBEMAP and DDSCAPS2_CUBEMAP_NEGATIVEX flags.
	 * </p>
	 * <p>
	 * The DDS_CUBEMAP_POSITIVEY flag, which is defined in Dds.h, is a bitwise-OR combination of the DDSCAPS2_CUBEMAP and DDSCAPS2_CUBEMAP_POSITIVEY flags.
	 * </p>
	 * <p>
	 * The DDS_CUBEMAP_NEGATIVEY flag, which is defined in Dds.h, is a bitwise-OR combination of the DDSCAPS2_CUBEMAP and DDSCAPS2_CUBEMAP_NEGATIVEY flags.
	 * </p>
	 * <p>
	 * The DDS_CUBEMAP_POSITIVEZ flag, which is defined in Dds.h, is a bitwise-OR combination of the DDSCAPS2_CUBEMAP and DDSCAPS2_CUBEMAP_POSITIVEZ flags.
	 * </p>
	 * <p>
	 * The DDS_CUBEMAP_NEGATIVEZ flag, which is defined in Dds.h, is a bitwise-OR combination of the DDSCAPS2_CUBEMAP and DDSCAPS2_CUBEMAP_NEGATIVEZ flags.
	 * </p>
	 * <p>
	 * The DDS_CUBEMAP_ALLFACES flag, which is defined in Dds.h, is a bitwise-OR combination of the DDS_CUBEMAP_POSITIVEX, DDS_CUBEMAP_NEGATIVEX, DDS_CUBEMAP_POSITIVEY,
	 * DDS_CUBEMAP_NEGATIVEY, DDS_CUBEMAP_POSITIVEZ, and DDSCAPS2_CUBEMAP_NEGATIVEZ flags.
	 * </p>
	 * <p>
	 * The DDS_FLAGS_VOLUME flag, which is defined in Dds.h, is equal to the DDSCAPS2_VOLUME flag.
	 * </p>
	 * <p class="note">
	 * <strong>Note</strong>&nbsp;&nbsp;Although Direct3D 9 supports partial cube-maps, Direct3D 10, 10.1, and 11 require that you define all six cube-map faces (that is, you must
	 * set DDS_CUBEMAP_ALLFACES).
	 * </p>
	 */
	public int cubemapFlags;
	
	public boolean isHeaderDX10;
	
	/**
	 * The surface pixel format
	 */
	public DXGI_FORMAT dxgiFormat;
	
	public int resourceDimension;
	
	public int miscFlag;
	
	/**
	 * <p>
	 * The number of elements in the array.
	 * </p>
	 * <p>
	 * For a <a href="http://msdn.microsoft.com/en-us/library/bb205133%28v=VS.85%29.aspx">2D texture</a> that is also a cube-map texture, this number represents the number of
	 * cubes. This number is the same as the number in the <strong>NumCubes</strong> member of <a href="http://msdn.microsoft.com/en-us/library/bb694536%28v=VS.85%29.aspx"><strong
	 * xmlns="http://www.w3.org/1999/xhtml">D3D10_TEXCUBE_ARRAY_SRV1</strong></a> or <a href="http://msdn.microsoft.com/en-us/library/ff476250%28v=VS.85%29.aspx"><strong
	 * xmlns="http://www.w3.org/1999/xhtml">D3D11_TEXCUBE_ARRAY_SRV</strong></a>). In this case, the DDS file contains <strong>arraySize</strong>*6 2D textures. For more
	 * information about this case, see the <strong>miscFlag</strong> description.
	 * </p>
	 * 
	 * <p>
	 * For a <a href="http://msdn.microsoft.com/en-us/library/bb205133%28v=VS.85%29.aspx">3D texture</a>, you must set this number to 1.
	 * </p>
	 */
	public int arraySize;
	
	public int compression;
	
	void read(DDSImageReader reader, ImageInputStream in) throws IOException
	{
		byte[] magic = new byte[4];
		in.readFully(magic, 0, 4);
		
		if (!((magic[0] == 'D') && (magic[1] == 'D') && (magic[2] == 'S') && (magic[3] == ' '))) {

		}
		
		int dwSize = in.readInt();
		if (dwSize != 124) {

		}
		
		headerFlags = in.readInt();
		
		if ((headerFlags & DDSD_REQUIRED) != DDSD_REQUIRED) {

		}
		
		height = in.readInt();
		width = in.readInt();
		pitchOrLinearSize = in.readInt();
		depth = in.readInt();
		mipMapCount = in.readInt();
		in.skipBytes(44);
		
		int pfSize = in.readInt();
		if (pfSize != 32) {

		}
		
		pfFlags = in.readInt();
		pfFourCC = in.readInt();
		pfRGBBitCount = in.readInt();
		pfRBitMask = in.readInt();
		pfGBitMask = in.readInt();
		pfBBitMask = in.readInt();
		pfABitMask = in.readInt();
		
		surfaceFlags = in.readInt();
		
		if ((surfaceFlags & DDSCAPS_TEXTURE) == 0) {

		}
		
		cubemapFlags = in.readInt();
		in.skipBytes(12);
		
		isHeaderDX10 = ((pfFlags & DDPF_FOURCC) != 0) && (pfFourCC == FOURCC_DX10);
		if (isHeaderDX10) {
			dxgiFormat = DXGI_FORMAT.values()[in.readInt()];
			resourceDimension = in.readInt();
			miscFlag = in.readInt();
			arraySize = in.readInt();
			in.skipBytes(4);
		}
	}
	
	public boolean isHeaderFlagSet(int flag)
	{
		return (headerFlags & flag) == flag;
	}
	
	public boolean isPixelFormatFlagSet(int flag)
	{
		return (pfFlags & flag) == flag;
	}
	
	public boolean isSurfaceFlagSet(int flag)
	{
		return (surfaceFlags & flag) == flag;
	}
	
	public boolean iscCubemapFlagSet(int flag)
	{
		return (cubemapFlags & flag) == flag;
	}
	
	void write(DDSImageWriter writer, ImageOutputStream out) throws IOException
	{
		byte[] magic = new byte[] { 'D', 'D', 'S', ' ' };
		out.write(magic, 0, 4);
		
		out.writeInt(124);
		out.writeInt(headerFlags);
		out.writeInt(height);
		out.writeInt(width);
		out.writeInt(pitchOrLinearSize);
		out.writeInt(depth);
		out.writeInt(mipMapCount);
		
		//reserved[11]
		out.skipBytes(44);
		
		out.writeInt(32);
		out.writeInt(pfFlags);
		out.writeInt(pfFourCC);
		out.writeInt(pfRGBBitCount);
		out.writeInt(pfRBitMask);
		out.writeInt(pfGBitMask);
		out.writeInt(pfBBitMask);
		out.writeInt(pfABitMask);
		
		out.writeInt(surfaceFlags);
		out.writeInt(cubemapFlags);
		
		out.skipBytes(12);
		
		if (isHeaderDX10) {
			out.writeInt(dxgiFormat.ordinal());
			out.writeInt(resourceDimension);
			out.writeInt(miscFlag);
			out.writeInt(arraySize);
			out.skipBytes(4);
		}
	}
	
	public long writtenSize()
	{
		return isHeaderDX10 ? 148 : 128;
	}
	
	public int mipMapWidth(int map)
	{
		int width = this.width;
		for (int i = 0; i < map; i++) {
			width >>= 1;
		}
		return Math.max(width, 1);
	}
	
	public int mipMapHeight(int map)
	{
		int height = this.height;
		for (int i = 0; i < map; i++) {
			height >>= 1;
		}
		return Math.max(height, 1);
	}
	
	public int mipMapDepth(int map)
	{
		int height = this.depth;
		for (int i = 0; i < map; i++) {
			height >>= 1;
		}
		return Math.max(height, 1);
	}
	
//	public long mipMapOffset(int map)
//	{
//		long offset = 0;
//		for (int i = 0; i < map; i++) {
//			offset += mipMapSizeInBytes(i);
//		}
//		return offset;
//	}
	
	public long imageOffset(int cubeMapFace, int mipMap)
	{
		long offset = 0;
		
		int numLevels = mipMapCount;
		if (numLevels == 0) {
			numLevels = 1;
		}
		
		int sideSize = 0;
		for (int i = 0; i < numLevels; i++) {
			int mipMapSize = mipMapSizeInBytes(i);
			sideSize += mipMapSize;
			if (i < mipMap) {
				offset += mipMapSize;
			}
		}
		for (int i = 0; i < cubeMapFace; i++) {
			offset += sideSize;
		}
		
		return offset;
	}
	
	public int mipMapSizeInBytes(int map)
	{
		int width = mipMapWidth(map);
		int height = mipMapHeight(map);
		int depth = mipMapDepth(map);
		if (isPixelFormatFlagSet(DDSConstants.DDPF_FOURCC)) {
			int blockSize = (pfFourCC == DDSConstants.D3DFMT_DXT1 ? 8 : 16);
			return ((width + 3) / 4) * ((height + 3) / 4) * depth * blockSize;
		} else
			return width * height * depth * (pfRGBBitCount / 8);
	}
	
	public int cubemapSideSizeInBytes()
	{
		int numLevels = mipMapCount;
		if (numLevels == 0) {
			numLevels = 1;
		}
		
		int size = 0;
		for (int i = 0; i < numLevels; i++) {
			size += mipMapSizeInBytes(i);
		}
		
		return size;
	}
	
	public Collection<ImageTypeSpecifier> getImageTypes()
	{
		compression = 0;
		if (isPixelFormatFlagSet(DDSConstants.DDPF_FOURCC)) {
			ImageTypeSpecifier nativeFormat = null;
			int imageType = 0;
			//			if (!hasAlpha) {
			//				imageType = BufferedImage.TYPE_INT_RGB;
			//				nativeFormat = ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0, 0xf800, 0,
			//						DataBuffer.TYPE_USHORT, true);
			//			} else
			switch (pfFourCC) {
			case DDSConstants.D3DFMT_DXT1:
				imageType = BufferedImage.TYPE_INT_ARGB_PRE;
				nativeFormat = ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0, 0xf800, 0x10000,
						DataBuffer.TYPE_INT, true);
				compression = DDSConstants.D3DFMT_DXT1;
				break;
			case DDSConstants.D3DFMT_DXT2:
				imageType = BufferedImage.TYPE_INT_ARGB_PRE;
				nativeFormat = ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0, 0xf800, 0xf0000,
						DataBuffer.TYPE_INT, true);
				compression = DDSConstants.D3DFMT_DXT2;
				break;
			case DDSConstants.D3DFMT_DXT3:
				imageType = BufferedImage.TYPE_INT_ARGB;
				nativeFormat = ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0, 0xf800, 0xf0000,
						DataBuffer.TYPE_INT, false);
				compression = DDSConstants.D3DFMT_DXT3;
				break;
			case DDSConstants.D3DFMT_DXT4:
				imageType = BufferedImage.TYPE_INT_ARGB_PRE;
				nativeFormat = ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0, 0xf800, 0xff0000,
						DataBuffer.TYPE_INT, true);
				compression = DDSConstants.D3DFMT_DXT4;
				break;
			case DDSConstants.D3DFMT_DXT5:
				imageType = BufferedImage.TYPE_INT_ARGB;
				nativeFormat = ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0, 0xf800, 0xff0000,
						DataBuffer.TYPE_INT, false);
				compression = DDSConstants.D3DFMT_DXT5;
				break;
			case DDSConstants.D3DFMT_A16B16G16R16:
				return Arrays.asList(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 0, 1, 2, 3 },
						DataBuffer.TYPE_USHORT, true, false));
			case D3DFMT_A32B32G32R32F:
				return Arrays.asList(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 0, 1, 2, 3 },
						DataBuffer.TYPE_FLOAT, true, false));
			case DDSConstants.FOURCC_DX10:
				//return dxgiFormat.getImageTypeSpecifiers();
			}
			
			return Arrays.asList(ImageTypeSpecifier.createFromBufferedImageType(imageType), nativeFormat);
		}
		
		boolean hasAlpha = isPixelFormatFlagSet(DDSConstants.DDPF_ALPHA | DDSConstants.DDPF_ALPHAPIXELS);
		
		ColorSpace cs = null;
		int transferType = DataBuffer.TYPE_BYTE;
		boolean noDirectCM = false;
		
		int numbits = pfRGBBitCount;
		switch (numbits) {
		case 24:
			noDirectCM = true;
		case 8:
			transferType = DataBuffer.TYPE_BYTE;
			break;
		case 16:
			transferType = DataBuffer.TYPE_USHORT;
			break;
		case 32:
			transferType = DataBuffer.TYPE_INT;
			break;
		}
		
		int nComponents;
		
		int rmask;
		int gmask;
		int bmask;
		int[] nBits, masks;
		if (isPixelFormatFlagSet(DDSConstants.DDPF_RGB)) {
			cs = ColorSpace.getInstance(ColorSpace.CS_sRGB);
			
			rmask = pfRBitMask;
			gmask = pfGBitMask;
			bmask = pfBBitMask;
			nComponents = 3;
		} else if (isPixelFormatFlagSet(DDSConstants.DDPF_LUMINANCE)) {
			cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
			
			rmask = pfRBitMask;
			gmask = pfRBitMask;
			bmask = pfRBitMask;
			nComponents = 1;
			
			noDirectCM = true;
		} else
			return Collections.<ImageTypeSpecifier> emptyList();
		
		if (hasAlpha) {
			masks = new int[nComponents + 1];
			masks[nComponents] = pfABitMask;
			nBits = new int[nComponents + 1];
		} else {
			masks = new int[nComponents];
			nBits = new int[nComponents];
		}
		
		switch (nComponents) {
		case 3:
			masks[2] = bmask;
		case 2:
			masks[1] = gmask;
		case 1:
			masks[0] = rmask;
			break;
		}
		
		int maxBandSize = 0;
		Arrays.sort(masks);
		for (int i = 0; i < masks.length; i++) {
			nBits[i] = countBits(masks[i]);
			if (maxBandSize < nBits[i]) {
				maxBandSize = nBits[i];
			}
		}
		
		int ccmTransferType = 0;
		
		if (maxBandSize == 8) {
			ccmTransferType = DataBuffer.TYPE_BYTE;
		} else if (maxBandSize == 16) {
			ccmTransferType = DataBuffer.TYPE_USHORT;
		} else if (maxBandSize == 8) {
			ccmTransferType = DataBuffer.TYPE_INT;
		} else {

		}
		
		ComponentColorModel ccm = new ComponentColorModel(cs, nBits, hasAlpha, false, hasAlpha ? Transparency.TRANSLUCENT : Transparency.OPAQUE,
				ccmTransferType);
		ImageTypeSpecifier ccmDesc = new ImageTypeSpecifier(ccm, ccm.createCompatibleSampleModel(1, 1));
		
		if (noDirectCM)
			return Arrays.asList(ccmDesc);
		
		return Arrays.asList(
				ImageTypeSpecifier.createPacked(cs, rmask, gmask, bmask, pfABitMask, transferType, false),
				ccmDesc
				);
	}
	
	public void init(ColorModel colorModel, SampleModel sampleModel, int width, int height, int compression, boolean dx10)
	{
		headerFlags = DDSD_REQUIRED;
		surfaceFlags = DDSCAPS_TEXTURE;
		this.width = width;
		this.height = height;
		this.compression = compression;
		
		boolean hasAlpha = colorModel.hasAlpha();
		
		if (compression == 0) {
			if (colorModel instanceof DirectColorModel) {
				DirectColorModel direct = (DirectColorModel) colorModel;
				pfRGBBitCount = direct.getPixelSize();
				pfFlags |= DDPF_RGB;
				
				pfRBitMask = direct.getRedMask();
				pfGBitMask = direct.getGreenMask();
				pfBBitMask = direct.getBlueMask();
				if (hasAlpha) {
					pfFlags |= DDPF_ALPHA;
					pfABitMask = direct.getAlphaMask();
				}
			} else if (colorModel instanceof ComponentColorModel) {
				ComponentColorModel comp = (ComponentColorModel) colorModel;
				ColorSpace cs = comp.getColorSpace();
				int[] components = comp.getComponentSize();
				int bitOffs = 0;
				
				if (cs.getType() == ColorSpace.TYPE_RGB) {
					pfFlags |= DDPF_RGB;
					pfRBitMask = makeMask(bitOffs, components[0]);
					bitOffs += components[0];
					pfGBitMask = makeMask(bitOffs, components[1]);
					bitOffs += components[1];
					pfBBitMask = makeMask(bitOffs, components[2]);
					bitOffs += components[2];
				} else if (cs.getType() == ColorSpace.TYPE_GRAY) {
					pfFlags |= DDPF_LUMINANCE;
					pfRBitMask = makeMask(bitOffs, components[0]);
					bitOffs += components[0];
				}
				if (hasAlpha) {
					pfFlags |= DDPF_ALPHA;
					pfABitMask = makeMask(bitOffs, components[cs.getNumComponents()]);
				}
			}
		} else {
			pfFlags |= DDPF_FOURCC;
			pfFourCC = compression;
		}
	}
	
	private static int makeMask(int bitOffs, int nBits)
	{
		int mask = 0;
		for (int i = bitOffs; i < bitOffs + nBits; i++) {
			mask |= 1 << i;
		}
		return mask;
	}
	
	public boolean isCompatible(ColorModel colorModel, SampleModel sampleModel)
	{
		ImageTypeSpecifier search = new ImageTypeSpecifier(colorModel, sampleModel);
		Iterator<ImageTypeSpecifier> it = getImageTypes().iterator();
		while (it.hasNext()) {
			ImageTypeSpecifier specifier = it.next();
			if (search.equals(specifier))
				return true;
		}
		return false;
	}
	
	static int countBits(int mask)
	{
		int count = 0;
		for (int i = 0; i < 32; i++) {
			int test = 1 << i;
			boolean success = (test & mask) != 0;
			if (success) {
				count++;
			} else if (count > 0) {
				break;
			}
		}
		return count;
	}
	
}
