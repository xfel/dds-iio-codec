/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import gr.zdimensions.jsquish.Squish;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.SinglePixelPackedSampleModel;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.Collection;
import java.util.Iterator;

import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

public class DDSImageReader extends ImageReader implements DDSConstants
{
	
	DDSFileHeader header;
	private ImageInputStream stream;
	private int width;
	private int height;
	private BufferedImage image;
	private int[] sourceBands;
	private int[] destBands;
	private Rectangle sourceRegion;
	private Rectangle destinationRegion;
	private int periodX;
	private int periodY;
	private boolean seleBand;
	private boolean noTransform;
	private boolean hasAlpha;
	private SampleModel sampleModel;
	private int numBands;
	private Collection<ImageTypeSpecifier> imageTypes;
	
	DDSImageReader(ImageReaderSpi originatingProvider)
	{
		super(originatingProvider);
	}
	
	@Override
	public boolean canReadRaster()
	{
		return true;
	}
	
	private void checkIndex(int imageIndex) throws IOException
	{
		if (imageIndex > getNumImages(false))
			throw new IndexOutOfBoundsException();
		if (imageIndex < minIndex)
			throw new IndexOutOfBoundsException();
	}
	
	@Override
	public int getHeight(int imageIndex) throws IOException
	{
		checkIndex(imageIndex);
		readHeader();
		return header.height;//mipMapHeight(imageIndex);
	}
	
	@Override
	public int getWidth(int imageIndex) throws IOException
	{
		checkIndex(imageIndex);
		readHeader();
		return header.width;//mipMapHeight(imageIndex);
	}
	
	@Override
	public IIOMetadata getImageMetadata(int imageIndex) throws IOException
	{
		checkIndex(imageIndex);
		
		return null;
	}
	
	@Override
	public boolean readerSupportsThumbnails()
	{
		return true;
	}
	
	@Override
	public int getThumbnailHeight(int imageIndex, int thumbnailIndex) throws IOException
	{
		checkIndex(imageIndex);
		readHeader();
		return header.mipMapHeight(thumbnailIndex + 1);
	}
	
	@Override
	public int getThumbnailWidth(int imageIndex, int thumbnailIndex) throws IOException
	{
		checkIndex(imageIndex);
		readHeader();
		return header.mipMapWidth(thumbnailIndex + 1);
	}
	
	@Override
	public int getNumThumbnails(int imageIndex) throws IOException
	{
		checkIndex(imageIndex);
		readHeader();
		if (!header.isHeaderFlagSet(DDSD_MIPMAPCOUNT))
			return 0;
		return header.mipMapCount - 1;
	}
	
	@Override
	public Iterator<ImageTypeSpecifier> getImageTypes(int imageIndex)
			throws IOException
	{
		checkIndex(imageIndex);
		readHeader();
		return imageTypes.iterator();
	}
	
	@Override
	public int getNumImages(boolean allowSearch) throws IOException
	{
		//readHeader();
		//if (!header.isSurfaceFlagSet(DDSCAPS_COMPLEX))
		return 1;
		//return header.mipMapCount;
	}
	
	@Override
	public IIOMetadata getStreamMetadata() throws IOException
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isRandomAccessEasy(int imageIndex) throws IOException
	{
		return true;
	}
	
	void readHeader() throws IOException
	{
		if (header != null)
			return;
		header = new DDSFileHeader();
		//stream.seek(0);
		header.read(this, stream);
		
//		if (seekForwardOnly) {
		stream.flush();
//		}
		
		hasAlpha = header.isPixelFormatFlagSet(DDPF_ALPHA | DDPF_ALPHAPIXELS);
		
		if (header.isPixelFormatFlagSet(DDPF_LUMINANCE)) {
			numBands = 1;
		} else {
			numBands = 3;
		}
		
		if (hasAlpha) {
			numBands++;
		}
		
		imageTypes = header.getImageTypes();
	}
	
	@Override
	public BufferedImage readThumbnail(int imageIndex, int thumbnailIndex) throws IOException
	{
		checkIndex(imageIndex);
		
		clearAbortRequest();
		processThumbnailStarted(imageIndex, thumbnailIndex);
		
		readHeader();
		
		width = header.mipMapWidth(thumbnailIndex + 1);
		height = header.mipMapHeight(thumbnailIndex + 1);
		image = getDestination(null, imageTypes.iterator(), width, height);
		
		sourceRegion = new Rectangle(0, 0, 0, 0);
		destinationRegion = new Rectangle(0, 0, 0, 0);
		computeRegions(null, width, height, image,
				sourceRegion, destinationRegion);
		
		periodX = 1;
		periodY = 1;
		
		// If the destination band is set use it
		
		noTransform = true;
		seleBand = false;
		sourceBands = new int[numBands];
		destBands = new int[numBands];
		for (int i = 0; i < numBands; i++) {
			destBands[i] = sourceBands[i] = i;
		}
		
		sampleModel = image.getSampleModel();
		
		//		if (seleBand) {
		//			sampleModel = sampleModel.createSubsetSampleModel(sourceBands);
		//		}
		
		readImage(imageIndex, thumbnailIndex + 1, true);
		
		if (abortRequested()) {
			processReadAborted();
		} else {
			processThumbnailComplete();
		}
		
		return image;
	}
	
	@Override
	public BufferedImage read(int imageIndex, ImageReadParam param) throws IOException
	{
		checkIndex(imageIndex);
		
		if (param == null) {
			param = getDefaultReadParam();
		}
		
		clearAbortRequest();
		processImageStarted(imageIndex);
		
		readHeader();
		
		width = header.width;//mipMapWidth(imageIndex);
		height = header.height;//mipMapHeight(imageIndex);
		image = getDestination(param, imageTypes.iterator(), width, height);
		
		sourceRegion = new Rectangle(0, 0, 0, 0);
		destinationRegion = new Rectangle(0, 0, 0, 0);
		computeRegions(param, width, height, image,
				sourceRegion, destinationRegion);
		
		periodX = param.getSourceXSubsampling();
		periodY = param.getSourceYSubsampling();
		
		//Try to use a smaller MipMap Image which fits better
		int mipMap = 0;
		
		int pX0, pY0;
		while (((pX0 = periodX / 2) * 2 == periodX) && ((pY0 = periodY / 2) * 2 == periodY) && (mipMap < header.mipMapCount)) {
			periodX = pX0;
			periodY = pY0;
			sourceRegion.width /= 2;
			sourceRegion.height /= 2;
			width /= 2;
			height /= 2;
			mipMap++;
		}
		
		// If the destination band is set used it
		sourceBands = param.getSourceBands();
		destBands = param.getDestinationBands();
		
		seleBand = (sourceBands != null) && (destBands != null);
		noTransform =
				destinationRegion.equals(new Rectangle(0, 0, width, height)) ||
						seleBand;
		
		if (!seleBand) {
			sourceBands = new int[numBands];
			destBands = new int[numBands];
			for (int i = 0; i < numBands; i++) {
				destBands[i] = sourceBands[i] = i;
			}
		}
		
		sampleModel = image.getSampleModel();
		
//		if (seleBand) {
//			sampleModel = sampleModel.createSubsetSampleModel(sourceBands);
//		}
		
		readImage(imageIndex, mipMap, false);
		
		if (abortRequested()) {
			processReadAborted();
		} else {
			processImageComplete();
		}
		
		return image;
	}
	
	protected void readImage(int imageIndex, int mipMap, boolean isThumbnail) throws IOException
	{
		stream.seek(header.imageOffset(imageIndex, mipMap) + header.writtenSize());
		
		if (header.isPixelFormatFlagSet(DDPF_RGB) || header.isPixelFormatFlagSet(DDPF_YUV) || header.isPixelFormatFlagSet(DDPF_LUMINANCE)) {
			int numDataElements = sampleModel.getNumDataElements();
			DataBuffer dataBuffer = image.getWritableTile(0, 0).getDataBuffer();
			switch (dataBuffer.getDataType()) {
			case DataBuffer.TYPE_BYTE:
				readRawData(((DataBufferByte) dataBuffer).getData(0), numDataElements, isThumbnail);
				break;
			case DataBuffer.TYPE_USHORT:
				readRawData(((DataBufferUShort) dataBuffer).getData(0), numDataElements, isThumbnail);
				break;
			case DataBuffer.TYPE_SHORT:
				readRawData(((DataBufferShort) dataBuffer).getData(0), numDataElements, isThumbnail);
				break;
			case DataBuffer.TYPE_INT:
				readRawData(((DataBufferInt) dataBuffer).getData(0), numDataElements, isThumbnail);
				break;
			case DataBuffer.TYPE_FLOAT:
				readRawData(((DataBufferFloat) dataBuffer).getData(0), numDataElements, isThumbnail);
				break;
			}
//			switch (header.pfRGBBitCount) {
//			case 8:
//				readRGB8Bit(((DataBufferByte) dataBuffer).getData(0), isThumbnail);
//				break;
//			case 16:
//				readRGB16Bit(((DataBufferUShort) dataBuffer).getData(0), isThumbnail);
//				break;
//			case 24:
//				readRGB24Bit(((DataBufferByte) dataBuffer).getData(0), isThumbnail);
//				break;
//			case 32:
//				readRGB32Bit(((DataBufferInt) dataBuffer).getData(0), isThumbnail);
//				break;
//			}
		} else if (header.compression != 0) {
			switch (header.compression) {
			case D3DFMT_DXT1:
				readS3tc(Squish.CompressionType.DXT1, isThumbnail);
				break;
			case D3DFMT_DXT2:
//				readS3tc(false, true, false);
//				break;
			case D3DFMT_DXT3:
				readS3tc(Squish.CompressionType.DXT3, isThumbnail);
				break;
			case D3DFMT_DXT4:
//				readS3tc(false, false, true);
//				break;
			case D3DFMT_DXT5:
				readS3tc(Squish.CompressionType.DXT5, isThumbnail);
				break;
			}
		}
		if (seekForwardOnly) {
			minIndex = imageIndex;
			stream.flushBefore(header.imageOffset(imageIndex, 0) + header.writtenSize());
		}
	}
	
	private void readS3tc(Squish.CompressionType compressionType, boolean isThumbnail) throws IOException
	{
		int blockSize = compressionType.blockSize;
		
		byte[] compressed = new byte[blockSize];
		byte[] uncompressed = new byte[64];
		
//		int numTilesHigh = (height + 3) / 4;
		int numTilesWide = (width + 3) / 4;
		
		int destMinX = destinationRegion.x;
		int destMinY = destinationRegion.y;
		int srcMinX = sourceRegion.x;
		int srcMinY = sourceRegion.y;
		
		int minTileX = srcMinX / 4;
		int minTileY = srcMinY / 4;
		int maxTileX = (srcMinX + sourceRegion.width + 3) / 4;
		int maxTileY = (srcMinY + sourceRegion.height + 3) / 4;
		
		if ((minTileX > 0) || (minTileY > 0)) {
			stream.skipBytes((numTilesWide * minTileY + minTileX) * blockSize);
		}
		
		int skipLength = 0;
		if (maxTileX < numTilesWide) {
			skipLength = (numTilesWide - maxTileX + minTileX) * blockSize;
		}
		
		int maxProgress = (maxTileX - minTileX) * (maxTileY - minTileY);
		int curProgress = 0;
		
		int srcY = srcMinY;
		int destY = destMinY;
		for (int tileY = minTileY; tileY < maxTileY; tileY++) {
			if (abortRequested())
				return;
			
			if (srcY >= tileY * 4 + 4) {
				stream.skipBytes(blockSize * numTilesWide);
				continue;
			}
			
			int srcX = srcMinX;
			int destX = destMinX;
			for (int tileX = minTileX; tileX < maxTileX; tileX++) {
				if (abortRequested())
					return;
				
				if (srcX >= tileX * 4 + 4) {
					stream.skipBytes(blockSize);
					continue;
				}
				
				stream.readFully(compressed, 0, blockSize);
				
				Squish.decompress(uncompressed, compressed, 0, compressionType);
				
				int destY0 = destY;
				int destX0 = destX;
				
				for (int sy = srcY; (sy < tileY * 4 + 4) && (destY0 < destinationRegion.height); sy += periodY, destY0++) {
					int offsY = (sy % 4) * 16;
					destX0 = destX;
					for (int sx = srcX; (sx < tileX * 4 + 4) && (destX0 < destinationRegion.width); sx += periodX, destX0++) {
						int offset = offsY + (sx % 4) * 4;
						int pixel = 0;
						pixel |= (uncompressed[offset + 2] & 0xFF) << 0;
						pixel |= (uncompressed[offset + 1] & 0xFF) << 8;
						pixel |= (uncompressed[offset + 0] & 0xFF) << 16;
						pixel |= (uncompressed[offset + 3] & 0xFF) << 24;
						
						image.setRGB(destX0, destY0, pixel);
					}
				}
				curProgress++;
				if (isThumbnail) {
					processThumbnailUpdate(image, destX, destY, destX0 - destX, destY0 - destY, 1, 1, new int[] { 0 });
					processThumbnailProgress(100f * curProgress / maxProgress);
				} else {
					processImageUpdate(image, destX, destY, destX0 - destX, destY0 - destY, 1, 1, new int[] { 0 });
					processImageProgress(100f * curProgress / maxProgress);
				}
				while (srcX < tileX * 4 + 4) {
					srcX += periodX;
					destX++;
				}
			}
			while (srcY < tileY * 4 + 4) {
				srcY += periodY;
				destY++;
			}
			stream.skipBytes(skipLength);
		}
	}
	
	@SuppressWarnings("unused")
	@Deprecated
	private void readRGB32Bit(int[] data, boolean isThumbnail) throws IOException
	{
		if (noTransform) {
			int j = 0;
			
			for (int i = 0; i < height; i++) {
				if (abortRequested()) {
					break;
				}
				stream.readFully(data, j, width);
				j += width;
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * i / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * i / destinationRegion.height);
				}
			}
		} else {
			int[] buf = new int[width];
			int lineStride =
					((SinglePixelPackedSampleModel) sampleModel).getScanlineStride();
			
			stream.skipBytes(width * sourceRegion.y << 2);
			
			int skipLength = width * (periodY - 1) << 2;
			
			int k = destinationRegion.y * lineStride;
			k += destinationRegion.x;
			
			for (int j = 0, y = sourceRegion.y; j < destinationRegion.height; j++, y += periodY) {
				
				if (abortRequested()) {
					break;
				}
				stream.readFully(buf, 0, width);
				for (int i = 0, m = sourceRegion.x; i < destinationRegion.width; i++, m += periodX) {
					//get the bit and assign to the data buffer of the raster
					data[k + i] = buf[m];
				}
				
				k += lineStride;
				stream.skipBytes(skipLength);
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * j / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * j / destinationRegion.height);
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	@Deprecated
	private void readRGB24Bit(byte[] data, boolean isThumbnail) throws IOException
	{
		int lineStride = width * 3;
		
		if (noTransform) {
			int j = 0;
			
			for (int i = 0; i < height; i++) {
				if (abortRequested()) {
					break;
				}
				stream.readFully(data, j, lineStride);
				j += lineStride;
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * i / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * i / destinationRegion.height);
				}
			}
		} else {
			byte[] buf = new byte[lineStride];
			lineStride =
					((ComponentSampleModel) sampleModel).getScanlineStride();
			
			stream.skipBytes(lineStride * sourceRegion.y);
			
			int skipLength = lineStride * (periodY - 1);
			
			int k = destinationRegion.y * lineStride;
			k += destinationRegion.x * 3;
			
			for (int j = 0, y = sourceRegion.y; j < destinationRegion.height; j++, y += periodY) {
				
				if (abortRequested()) {
					break;
				}
				stream.read(buf, 0, lineStride);
				for (int i = 0, m = 3 * sourceRegion.x; i < destinationRegion.width; i++, m += 3 * periodX) {
					//get the bit and assign to the data buffer of the raster
					int n = 3 * i + k;
					for (int b = 0; b < destBands.length; b++) {
						data[n + destBands[b]] = buf[m + sourceBands[b]];
					}
				}
				
				k += lineStride;
				stream.skipBytes(skipLength);
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * j / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * j / destinationRegion.height);
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	@Deprecated
	private void readRGB16Bit(short[] data, boolean isThumbnail) throws IOException
	{
		if (noTransform) {
			int j = 0;
			for (int i = 0; i < height; i++) {
				if (abortRequested()) {
					break;
				}
				
				stream.readFully(data, j, width);
				
				j += width;
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * i / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * i / destinationRegion.height);
				}
			}
		} else {
			short[] buf = new short[width];
			int lineStride =
					((SinglePixelPackedSampleModel) sampleModel).getScanlineStride();
			
			stream.skipBytes(width * sourceRegion.y << 1);
			
			int skipLength = width * (periodY - 1) << 1;
			
			int k = destinationRegion.y * lineStride;
			k += destinationRegion.x;
			
			for (int j = 0, y = sourceRegion.y; j < destinationRegion.height; j++, y += periodY) {
				
				if (abortRequested()) {
					break;
				}
				stream.readFully(buf, 0, width);
				for (int i = 0, m = sourceRegion.x; i < destinationRegion.width; i++, m += periodX) {
					//get the bit and assign to the data buffer of the raster
					data[k + i] = buf[m];
				}
				
				k += lineStride;
				stream.skipBytes(skipLength);
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * j / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * j / destinationRegion.height);
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	@Deprecated
	private void readRGB8Bit(byte[] data, boolean isThumbnail) throws IOException
	{
		if (noTransform) {
			int j = 0;
			
			for (int i = 0; i < height; i++) {
				if (abortRequested()) {
					break;
				}
				stream.readFully(data, j, width);
				j += width;
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * i / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * i / destinationRegion.height);
				}
			}
		} else {
			byte[] buf = new byte[width];
			int lineStride =
					((ComponentSampleModel) sampleModel).getScanlineStride();
			
			stream.skipBytes(width * sourceRegion.y);
			
			int skipLength = width * (periodX - 1);
			
			int k = destinationRegion.y * lineStride;
			
			k += destinationRegion.x;
			
			for (int j = 0, y = sourceRegion.y; j < destinationRegion.height; j++, y += periodX) {
				
				if (abortRequested()) {
					break;
				}
				stream.read(buf, 0, width);
				for (int i = 0, m = sourceRegion.x; i < destinationRegion.width; i++, m += periodY) {
					//get the bit and assign to the data buffer of the raster
					data[k + i] = buf[m];
				}
				
				k += lineStride;
				stream.skipBytes(skipLength);
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * j / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * j / destinationRegion.height);
				}
			}
		}
	}
	
	private void readRawData(byte[] data, int numDataElements, boolean isThumbnail) throws IOException
	{
		int lineStride = width * numDataElements;
		
		if (noTransform) {
			int j = 0;
			
			for (int i = 0; i < height; i++) {
				if (abortRequested()) {
					break;
				}
				stream.readFully(data, j, lineStride);
				j += lineStride;
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * i / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * i / destinationRegion.height);
				}
			}
		} else {
			byte[] buf = new byte[lineStride];
			if (sampleModel instanceof ComponentSampleModel) {
				lineStride = ((ComponentSampleModel) sampleModel).getScanlineStride();
			} else {
				lineStride = ((SinglePixelPackedSampleModel) sampleModel).getScanlineStride();
			}
			stream.skipBytes(lineStride * sourceRegion.y);
			
			int skipLength = lineStride * (periodY - 1);
			
			int k = destinationRegion.y * lineStride;
			k += destinationRegion.x * numDataElements;
			
			for (int j = 0, y = sourceRegion.y; j < destinationRegion.height; j++, y += periodY) {
				
				if (abortRequested()) {
					break;
				}
				stream.read(buf, 0, lineStride);
				for (int i = 0, m = numDataElements * sourceRegion.x; i < destinationRegion.width; i++, m += numDataElements * periodX) {
					//get the bit and assign to the data buffer of the raster
					int n = numDataElements * i + k;
					for (int b = 0; b < destBands.length; b++) {
						data[n + destBands[b]] = buf[m + sourceBands[b]];
					}
				}
				
				k += lineStride;
				stream.skipBytes(skipLength);
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * j / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * j / destinationRegion.height);
				}
			}
		}
	}
	
	private void readRawData(short[] data, int numDataElements, boolean isThumbnail) throws IOException
	{
		int lineStride = width * numDataElements;
		
		if (noTransform) {
			int j = 0;
			
			for (int i = 0; i < height; i++) {
				if (abortRequested()) {
					break;
				}
				stream.readFully(data, j, lineStride);
				j += lineStride;
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * i / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * i / destinationRegion.height);
				}
			}
		} else {
			byte[] buf = new byte[lineStride];
			if (sampleModel instanceof ComponentSampleModel) {
				lineStride = ((ComponentSampleModel) sampleModel).getScanlineStride();
			} else {
				lineStride = ((SinglePixelPackedSampleModel) sampleModel).getScanlineStride();
			}
			stream.skipBytes(lineStride * sourceRegion.y);
			
			int skipLength = lineStride * (periodY - 1);
			
			int k = destinationRegion.y * lineStride;
			k += destinationRegion.x * numDataElements;
			
			for (int j = 0, y = sourceRegion.y; j < destinationRegion.height; j++, y += periodY) {
				
				if (abortRequested()) {
					break;
				}
				stream.read(buf, 0, lineStride);
				for (int i = 0, m = numDataElements * sourceRegion.x; i < destinationRegion.width; i++, m += numDataElements * periodX) {
					//get the bit and assign to the data buffer of the raster
					int n = numDataElements * i + k;
					for (int b = 0; b < destBands.length; b++) {
						data[n + destBands[b]] = buf[m + sourceBands[b]];
					}
				}
				
				k += lineStride;
				stream.skipBytes(skipLength);
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * j / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * j / destinationRegion.height);
				}
			}
		}
	}
	
	private void readRawData(int[] data, int numDataElements, boolean isThumbnail) throws IOException
	{
		int lineStride = width * numDataElements;
		
		if (noTransform) {
			int j = 0;
			
			for (int i = 0; i < height; i++) {
				if (abortRequested()) {
					break;
				}
				stream.readFully(data, j, lineStride);
				j += lineStride;
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * i / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * i / destinationRegion.height);
				}
			}
		} else {
			byte[] buf = new byte[lineStride];
			if (sampleModel instanceof ComponentSampleModel) {
				lineStride = ((ComponentSampleModel) sampleModel).getScanlineStride();
			} else {
				lineStride = ((SinglePixelPackedSampleModel) sampleModel).getScanlineStride();
			}
			stream.skipBytes(lineStride * sourceRegion.y);
			
			int skipLength = lineStride * (periodY - 1);
			
			int k = destinationRegion.y * lineStride;
			k += destinationRegion.x * numDataElements;
			
			for (int j = 0, y = sourceRegion.y; j < destinationRegion.height; j++, y += periodY) {
				
				if (abortRequested()) {
					break;
				}
				stream.read(buf, 0, lineStride);
				for (int i = 0, m = numDataElements * sourceRegion.x; i < destinationRegion.width; i++, m += numDataElements * periodX) {
					//get the bit and assign to the data buffer of the raster
					int n = numDataElements * i + k;
					for (int b = 0; b < destBands.length; b++) {
						data[n + destBands[b]] = buf[m + sourceBands[b]];
					}
				}
				
				k += lineStride;
				stream.skipBytes(skipLength);
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * j / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * j / destinationRegion.height);
				}
			}
		}
	}
	
	private void readRawData(float[] data, int numDataElements, boolean isThumbnail) throws IOException
	{
		int lineStride = width * numDataElements;
		
		if (noTransform) {
			int j = 0;
			
			for (int i = 0; i < height; i++) {
				if (abortRequested()) {
					break;
				}
				stream.readFully(data, j, lineStride);
				j += lineStride;
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * i / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, i,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * i / destinationRegion.height);
				}
			}
		} else {
			byte[] buf = new byte[lineStride];
			if (sampleModel instanceof ComponentSampleModel) {
				lineStride = ((ComponentSampleModel) sampleModel).getScanlineStride();
			} else {
				lineStride = ((SinglePixelPackedSampleModel) sampleModel).getScanlineStride();
			}
			stream.skipBytes(lineStride * sourceRegion.y);
			
			int skipLength = lineStride * (periodY - 1);
			
			int k = destinationRegion.y * lineStride;
			k += destinationRegion.x * numDataElements;
			
			for (int j = 0, y = sourceRegion.y; j < destinationRegion.height; j++, y += periodY) {
				
				if (abortRequested()) {
					break;
				}
				stream.read(buf, 0, lineStride);
				for (int i = 0, m = numDataElements * sourceRegion.x; i < destinationRegion.width; i++, m += numDataElements * periodX) {
					//get the bit and assign to the data buffer of the raster
					int n = numDataElements * i + k;
					for (int b = 0; b < destBands.length; b++) {
						data[n + destBands[b]] = buf[m + sourceBands[b]];
					}
				}
				
				k += lineStride;
				stream.skipBytes(skipLength);
				if (isThumbnail) {
					processThumbnailUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processThumbnailProgress(100.0F * j / destinationRegion.height);
				} else {
					processImageUpdate(image, 0, j,
							destinationRegion.width, 1, 1, 1,
							new int[] { 0 });
					processImageProgress(100.0F * j / destinationRegion.height);
				}
			}
		}
	}
	
	@Override
	public Raster readRaster(int imageIndex, ImageReadParam param)
			throws IOException
	{
		return read(imageIndex, param).getData();
	}
	
	@Override
	public void setInput(Object input, boolean seekForwardOnly,
			boolean ignoreMetadata)
	{
		super.setInput(input, seekForwardOnly, ignoreMetadata);
		if (input != null) {
			stream = (ImageInputStream) input;
			stream.setByteOrder(ByteOrder.LITTLE_ENDIAN);
		} else {
			stream = null;
		}
		resetStreamSettings();
	}
	
	private void resetStreamSettings()
	{
		header = null;
	}
	
	@Override
	public void reset()
	{
		super.reset();
		resetStreamSettings();
	}
	
}
