/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.SampleModel;
import java.util.Iterator;

import javax.imageio.ImageTypeSpecifier;

import org.xfel.imageio.plugins.utils.ArrayItrator;

public enum DXGI_FORMAT {
	DXGI_FORMAT_UNKNOWN,

	DXGI_FORMAT_R32G32B32A32_TYPELESS(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_INT, true, false)),

	DXGI_FORMAT_R32G32B32A32_FLOAT(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_FLOAT, true, false)),

	DXGI_FORMAT_R32G32B32A32_UINT(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_INT, true, false)),

	DXGI_FORMAT_R32G32B32A32_SINT(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_INT, true, false)),

	DXGI_FORMAT_R32G32B32_TYPELESS(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 2, 1, 0 }, DataBuffer.TYPE_INT, false, false)),

	DXGI_FORMAT_R32G32B32_FLOAT(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 2, 1, 0 }, DataBuffer.TYPE_FLOAT, false, false)),

	DXGI_FORMAT_R32G32B32_UINT(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 2, 1, 0 }, DataBuffer.TYPE_INT, false, false)),

	DXGI_FORMAT_R32G32B32_SINT(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 2, 1, 0 }, DataBuffer.TYPE_INT, false, false)),

	DXGI_FORMAT_R16G16B16A16_TYPELESS(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16,
			16, 16 }, new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R16G16B16A16_FLOAT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16,
			16, 16 }, new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_FLOAT, false)),

	DXGI_FORMAT_R16G16B16A16_UNORM(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16,
			16, 16 }, new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R16G16B16A16_UINT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16, 16,
			16 }, new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R16G16B16A16_SNORM(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16,
			16, 16 }, new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_SHORT, false)),

	DXGI_FORMAT_R16G16B16A16_SINT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16, 16,
			16 }, new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_SHORT, false)),

	DXGI_FORMAT_R32G32_TYPELESS(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 32, 32 },
			new int[] { 1, 0 }, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_R32G32_FLOAT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 32, 32 },
			new int[] { 1, 0 }, DataBuffer.TYPE_FLOAT, false)),

	DXGI_FORMAT_R32G32_UINT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 32, 32 },
			new int[] { 1, 0 }, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_R32G32_SINT,

	DXGI_FORMAT_R32G8X24_TYPELESS,

	DXGI_FORMAT_D32_FLOAT_S8X24_UINT,

	DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS,

	DXGI_FORMAT_X32_TYPELESS_G8X24_UINT,

	DXGI_FORMAT_R10G10B10A2_TYPELESS(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 10, 10,
			10, 2 }, new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_INT, true)),

	DXGI_FORMAT_R10G10B10A2_UNORM(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 10, 10, 10,
			2 }, new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_INT, true)),

	DXGI_FORMAT_R10G10B10A2_UINT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 10, 10, 10,
			2 }, new int[] { 3, 2, 1, 0 }, DataBuffer.TYPE_INT, true)),

	DXGI_FORMAT_R11G11B10_FLOAT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 10, 10, 10 },
			new int[] { 2, 1, 0 }, DataBuffer.TYPE_FLOAT, false)),

	DXGI_FORMAT_R8G8B8A8_TYPELESS(ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_ARGB)),

	DXGI_FORMAT_R8G8B8A8_UNORM(ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_ARGB)),

	DXGI_FORMAT_R8G8B8A8_UNORM_SRGB(ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_ARGB)),

	DXGI_FORMAT_R8G8B8A8_UINT(ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_ARGB)),

	DXGI_FORMAT_R8G8B8A8_SNORM,

	DXGI_FORMAT_R8G8B8A8_SINT,

	DXGI_FORMAT_R16G16_TYPELESS(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16 },
			new int[] { 1, 0 }, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_R16G16_FLOAT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16 },
			new int[] { 1, 0 }, DataBuffer.TYPE_FLOAT, false)),

	DXGI_FORMAT_R16G16_UNORM(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16 },
			new int[] { 1, 0 }, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R16G16_UINT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16 },
			new int[] { 1, 0 }, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R16G16_SNORM(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16 },
			new int[] { 1, 0 }, DataBuffer.TYPE_SHORT, false)),

	DXGI_FORMAT_R16G16_SINT(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 16, 16 },
			new int[] { 1, 0 }, DataBuffer.TYPE_SHORT, false)),

	DXGI_FORMAT_R32_TYPELESS(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] { 0 }, DataBuffer.TYPE_INT, false, false)),

	DXGI_FORMAT_D32_FLOAT(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] { 0 }, DataBuffer.TYPE_FLOAT, false, false)),

	DXGI_FORMAT_R32_FLOAT(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] { 0 }, DataBuffer.TYPE_FLOAT, false, false)),

	DXGI_FORMAT_R32_UINT(ImageTypeSpecifier.createInterleaved(ColorSpace.getInstance(ColorSpace.CS_GRAY),
			new int[] { 0 }, DataBuffer.TYPE_INT, false, false)),

	DXGI_FORMAT_R32_SINT, DXGI_FORMAT_R24G8_TYPELESS(createInterleaved(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			new int[] { 24, 8 }, new int[] { 1, 0 }, DataBuffer.TYPE_SHORT, false)),

	DXGI_FORMAT_D24_UNORM_S8_UINT,

	DXGI_FORMAT_R24_UNORM_X8_TYPELESS,

	DXGI_FORMAT_X24_TYPELESS_G8_UINT,

	DXGI_FORMAT_R8G8_TYPELESS(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0xff00, 0xff,
			0, 0, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R8G8_UNORM(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0xff00, 0xff, 0,
			0, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R8G8_UINT(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0xff00, 0xff, 0,
			0, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R8G8_SNORM,

	DXGI_FORMAT_R8G8_SINT,

	DXGI_FORMAT_R16_TYPELESS(ImageTypeSpecifier.createGrayscale(16, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R16_FLOAT,

	DXGI_FORMAT_D16_UNORM(ImageTypeSpecifier.createGrayscale(16, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R16_UNORM(ImageTypeSpecifier.createGrayscale(16, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R16_UINT(ImageTypeSpecifier.createGrayscale(16, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_R16_SNORM(ImageTypeSpecifier.createGrayscale(16, DataBuffer.TYPE_SHORT, true)),

	DXGI_FORMAT_R16_SINT(ImageTypeSpecifier.createGrayscale(16, DataBuffer.TYPE_SHORT, true)),

	DXGI_FORMAT_R8_TYPELESS(ImageTypeSpecifier.createGrayscale(8, DataBuffer.TYPE_BYTE, false)),

	DXGI_FORMAT_R8_UNORM(ImageTypeSpecifier.createGrayscale(8, DataBuffer.TYPE_BYTE, false)),

	DXGI_FORMAT_R8_UINT(ImageTypeSpecifier.createGrayscale(8, DataBuffer.TYPE_BYTE, false)),

	DXGI_FORMAT_R8_SNORM(ImageTypeSpecifier.createGrayscale(8, DataBuffer.TYPE_BYTE, true)),

	DXGI_FORMAT_R8_SINT(ImageTypeSpecifier.createGrayscale(8, DataBuffer.TYPE_BYTE, true)),

	DXGI_FORMAT_A8_UNORM(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0, 0, 0, 0xff,
			DataBuffer.TYPE_BYTE, false)),

	DXGI_FORMAT_R1_UNORM(ImageTypeSpecifier.createGrayscale(1, DataBuffer.TYPE_BYTE, true)),

	DXGI_FORMAT_R9G9B9E5_SHAREDEXP,

	DXGI_FORMAT_R8G8_B8G8_UNORM,

	DXGI_FORMAT_G8R8_G8B8_UNORM,

	DXGI_FORMAT_BC1_TYPELESS(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0,
			0xf800, 0x10000, DataBuffer.TYPE_INT, true)),

	DXGI_FORMAT_BC1_UNORM(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0,
			0xf800, 0x10000, DataBuffer.TYPE_INT, true)),

	DXGI_FORMAT_BC1_UNORM_SRGB(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0,
			0xf800, 0x10000, DataBuffer.TYPE_INT, true)),

	DXGI_FORMAT_BC2_TYPELESS(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0,
			0xf800, 0xf0000, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_BC2_UNORM(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0,
			0xf800, 0xf0000, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_BC2_UNORM_SRGB(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0,
			0xf800, 0xf0000, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_BC3_TYPELESS(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0,
			0xf800, 0xff0000, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_BC3_UNORM(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0,
			0xf800, 0xff0000, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_BC3_UNORM_SRGB(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x1f, 0x7e0,
			0xf800, 0xff0000, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_BC4_TYPELESS,

	DXGI_FORMAT_BC4_UNORM,

	DXGI_FORMAT_BC4_SNORM,

	DXGI_FORMAT_BC5_TYPELESS,

	DXGI_FORMAT_BC5_UNORM,

	DXGI_FORMAT_BC5_SNORM,

	DXGI_FORMAT_B5G6R5_UNORM(ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_USHORT_565_RGB)),

	DXGI_FORMAT_B5G5R5A1_UNORM(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x7C00,
			0x03E0, 0x001F, 0x8000, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_B8G8R8A8_UNORM(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x000000ff,
			0x0000ff00, 0x00ff0000, 0xff000000, DataBuffer.TYPE_USHORT, false)),

	DXGI_FORMAT_B8G8R8X8_UNORM(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB), 0x000000ff,
			0x0000ff00, 0x00ff0000, 0, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM,

	DXGI_FORMAT_B8G8R8A8_TYPELESS(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_B8G8R8A8_UNORM_SRGB(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_B8G8R8X8_TYPELESS(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			0x000000ff, 0x0000ff00, 0x00ff0000, 0, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_B8G8R8X8_UNORM_SRGB(ImageTypeSpecifier.createPacked(ColorSpace.getInstance(ColorSpace.CS_sRGB),
			0x000000ff, 0x0000ff00, 0x00ff0000, 0, DataBuffer.TYPE_INT, false)),

	DXGI_FORMAT_BC6H_TYPELESS,

	DXGI_FORMAT_BC6H_UF16,

	DXGI_FORMAT_BC6H_SF16,

	DXGI_FORMAT_BC7_TYPELESS,

	DXGI_FORMAT_BC7_UNORM,

	DXGI_FORMAT_BC7_UNORM_SRGB;

	public static DXGI_FORMAT get(int _dxgi_format) {
		return values()[_dxgi_format];
	}

	private static ImageTypeSpecifier createInterleaved(ColorSpace colorSpace, int[] numBits, int[] bandOffsets,
			int dataType, boolean hasAlpha) {

		int transparency = hasAlpha ? Transparency.TRANSLUCENT : Transparency.OPAQUE;

		ColorModel colorModel = new ComponentColorModel(colorSpace, numBits, hasAlpha, false, transparency, dataType);

		int minBandOffset = bandOffsets[0];
		int maxBandOffset = minBandOffset;
		for (int i = 0; i < bandOffsets.length; i++) {
			int offset = bandOffsets[i];
			minBandOffset = Math.min(offset, minBandOffset);
			maxBandOffset = Math.max(offset, maxBandOffset);
		}
		int pixelStride = maxBandOffset - minBandOffset + 1;

		int w = 1;
		int h = 1;
		SampleModel sampleModel = new PixelInterleavedSampleModel(dataType, w, h, pixelStride, w * pixelStride,
				bandOffsets);
		return new ImageTypeSpecifier(colorModel, sampleModel);
	}

	private final ImageTypeSpecifier[] spec;

	private DXGI_FORMAT(ImageTypeSpecifier... spec) {
		this.spec = spec;
	}

	private DXGI_FORMAT(ColorModel colorModel) {
		this(new ImageTypeSpecifier(colorModel, colorModel.createCompatibleSampleModel(1, 1)));
	}

	public Iterator<ImageTypeSpecifier> getImageTypeSpecifiers() {
		return new ArrayItrator<ImageTypeSpecifier>(spec);
	}

}
