/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import gr.zdimensions.jsquish.Squish;

import java.awt.Rectangle;
import java.awt.image.BandedSampleModel;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.MultiPixelPackedSampleModel;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.SampleModel;
import java.awt.image.SinglePixelPackedSampleModel;
import java.io.IOException;
import java.nio.ByteOrder;

import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;

public class DDSImageWriter extends ImageWriter
{
	
	private ImageOutputStream stream;
	private DDSFileHeader header;
	
	private SampleModel sampleModel;
	private ColorModel colorModel;
	private ColorModel destinationColorModel;
	
	private Raster inputRaster;
	private RenderedImage input;
	
	private boolean noTransform;
	private int compression;
	
	private Rectangle sourceRegion;
	private Rectangle destinationRegion;
	
	private int scaleX;
	private int scaleY;
	private int xOffset;
	private int yOffset;
	
	private int dataType;
	
	private boolean noSubband;
	private int numBands;
	private int[] sourceBands;
	
//	private int[] ipixels;
//	private short[] spixels;
	
	protected DDSImageWriter(ImageWriterSpi originatingProvider)
	{
		super(originatingProvider);
	}
	
	@Override
	public boolean canWriteRasters()
	{
		return true;
	}
	
	@Override
	public void reset()
	{
		clean();
		super.reset();
	}
	
	@Override
	public void setOutput(Object output)
	{
		super.setOutput(output); // validates output
		if (output != null) {
			this.stream = (ImageOutputStream) output;
			stream.setByteOrder(ByteOrder.LITTLE_ENDIAN);
		} else {
			this.stream = null;
		}
	}
	
	@Override
	public DDSImageWriteParam getDefaultWriteParam()
	{
		return new DDSImageWriteParam();
	}
	
	@Override
	public IIOMetadata convertImageMetadata(IIOMetadata inData,
			ImageTypeSpecifier imageType, ImageWriteParam param)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public IIOMetadata convertStreamMetadata(IIOMetadata inData,
			ImageWriteParam param)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public IIOMetadata getDefaultImageMetadata(ImageTypeSpecifier imageType,
			ImageWriteParam param)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public IIOMetadata getDefaultStreamMetadata(ImageWriteParam param)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void write(IIOMetadata streamMetadata, IIOImage image, ImageWriteParam param) throws IOException
	{
		clean();
		
		DDSImageWriteParam ddsParam;
		if (param instanceof DDSImageWriteParam) {
			ddsParam = (DDSImageWriteParam) param;
		} else {
			ddsParam = getDefaultWriteParam();
		}
		
		input = null;
		inputRaster = null;
		boolean writeRaster = image.hasRaster();
		sourceRegion = ddsParam.getSourceRegion();
		sampleModel = null;
		colorModel = null;
		
		if (ddsParam.getDestinationType() != null) {
			destinationColorModel = ddsParam.getDestinationType().getColorModel();
		} else {
			destinationColorModel = null;
		}
		
		if (writeRaster) {
			inputRaster = image.getRaster();
			sampleModel = inputRaster.getSampleModel();
//			colorModel = ImageUtil.createColorModel(null, sampleModel);
			if (sourceRegion == null) {
				sourceRegion = inputRaster.getBounds();
			} else {
				sourceRegion = sourceRegion.intersection(inputRaster.getBounds());
			}
		} else {
			input = image.getRenderedImage();
			sampleModel = input.getSampleModel();
			colorModel = input.getColorModel();
			Rectangle rect = new Rectangle(input.getMinX(), input.getMinY(),
					input.getWidth(), input.getHeight());
			if (sourceRegion == null) {
				sourceRegion = rect;
			} else {
				sourceRegion = sourceRegion.intersection(rect);
			}
		}
		
		if (destinationColorModel == null) {
			destinationColorModel = colorModel;
		}
		sourceBands = param.getSourceBands();
		noSubband = true;
		numBands = sampleModel.getNumBands();
		
		if (sourceBands != null) {
			sampleModel = sampleModel.createSubsetSampleModel(sourceBands);
			colorModel = null;
			noSubband = false;
			numBands = sampleModel.getNumBands();
		} else {
			sourceBands = new int[numBands];
			for (int i = 0; i < numBands; i++) {
				sourceBands[i] = i;
			}
		}
		
		if (sourceRegion.isEmpty())
			throw new RuntimeException();
		
		scaleX = ddsParam.getSourceXSubsampling();
		scaleY = ddsParam.getSourceYSubsampling();
		xOffset = ddsParam.getSubsamplingXOffset();
		yOffset = ddsParam.getSubsamplingYOffset();
		
		// cache the data type;
		dataType = sampleModel.getDataType();
		
		sourceRegion.translate(xOffset, yOffset);
		sourceRegion.width -= xOffset;
		sourceRegion.height -= yOffset;
		
		int minX = sourceRegion.x / scaleX;
		int minY = sourceRegion.y / scaleY;
		int width = (sourceRegion.width + scaleX - 1) / scaleX;
		int height = (sourceRegion.height + scaleY - 1) / scaleY;
		xOffset = sourceRegion.x % scaleX;
		yOffset = sourceRegion.y % scaleY;
		
		destinationRegion = new Rectangle(minX, minY, width, height);
		noTransform = destinationRegion.equals(sourceRegion);
		
//		compression = 0;
		
		switch (param.getCompressionMode()) {
		case ImageWriteParam.MODE_EXPLICIT:
			String ctn = param.getCompressionType();
			if ("DXT1".equalsIgnoreCase(ctn)) {
				compression = DDSConstants.D3DFMT_DXT1;
//			} else if ("DXT2".equalsIgnoreCase(ctn)) {
//				compression = DDSConstants.D3DFMT_DXT2;
			} else if ("DXT3".equalsIgnoreCase(ctn)) {
				compression = DDSConstants.D3DFMT_DXT3;
//			} else if ("DXT4".equalsIgnoreCase(ctn)) {
//				compression = DDSConstants.D3DFMT_DXT4;
			} else if ("DXT5".equalsIgnoreCase(ctn)) {
				compression = DDSConstants.D3DFMT_DXT5;
			}
			break;
		case ImageWriteParam.MODE_COPY_FROM_METADATA:

			break;
		case ImageWriteParam.MODE_DEFAULT:
		default:
			compression = 0;
			break;
		}
		
		if (header == null) {
			header = new DDSFileHeader();
			header.init(destinationColorModel, sampleModel, width, height, compression, false);
		} else {
			if ((compression == 0) && !header.isCompatible(destinationColorModel, sampleModel)) {
//				raster = makeCompatible(colorModel, sampleModel, raster);
			}
		}
		
		Raster raster = inputRaster;
		if (raster == null) {
			raster = input.getData(sourceRegion);
		}
		
		Raster[] mipMaps;
		if (image.getNumThumbnails() > 0) {
			header.headerFlags |= DDSConstants.DDSD_MIPMAPCOUNT;
			header.surfaceFlags |= DDSConstants.DDSCAPS_COMPLEX | DDSConstants.DDSCAPS_MIPMAP;
			header.mipMapCount = image.getNumThumbnails() + 1;
			mipMaps = new Raster[header.mipMapCount];
			mipMaps[0] = raster;
			for (int i = 1; i < mipMaps.length; i++) {
				BufferedImage map = image.getThumbnail(i - 1);
				if ((map.getWidth() != header.mipMapWidth(i)) || (map.getHeight() != header.mipMapHeight(i)))
					throw new IllegalArgumentException("Illegal MipMap size");
				if ((compression == 0) && !header.isCompatible(map.getColorModel(), map.getSampleModel()))
					throw new IllegalArgumentException("unsupported thumbnail image type");
//					mipMaps[i] = makeCompatible(map.getColorModel(), map.getSampleModel(), map.getData());
				mipMaps[i] = map.getData();
			}
		} else {
			mipMaps = new Raster[] { raster };
		}
		
		if (compression != 0) {
			switch (compression) {
			case DDSConstants.D3DFMT_DXT1:
				for (Raster r : mipMaps) {
					writeS3tc(r, Squish.CompressionType.DXT1, width, height);
				}
				break;
			case DDSConstants.D3DFMT_DXT2:
			case DDSConstants.D3DFMT_DXT3:
				for (Raster r : mipMaps) {
					writeS3tc(r, Squish.CompressionType.DXT3, width, height);
				}
				break;
			case DDSConstants.D3DFMT_DXT4:
			case DDSConstants.D3DFMT_DXT5:
				for (Raster r : mipMaps) {
					writeS3tc(r, Squish.CompressionType.DXT5, width, height);
				}
				break;
			}
			return;
		}
		writeUncompressed(width, height);
	}
	
	private void clean()
	{
		if (stream != null) {
			try {
				stream.flush();
			} catch (IOException e) {}
		}
		header = null;
	}
	
	private void writeS3tc(Raster raster, Squish.CompressionType compressionType, int width, int height) throws IOException
	{
		int blockSize = compressionType.blockSize;
		
		byte[] compressed = new byte[blockSize];
		byte[] uncompressed = new byte[64];
		
		int numTilesHigh = (height + 3) / 4;
		int numTilesWide = (width + 3) / 4;
		
		int x = sourceRegion.x;
		int y = sourceRegion.y;
		int maxX = sourceRegion.width + sourceRegion.x;
		int maxY = sourceRegion.height + sourceRegion.y;
		
		Rectangle tileBounds = new Rectangle();
		for (int tileY = 0; tileY < numTilesHigh; tileY++) {
			for (int tileX = 0; tileX < numTilesWide; tileX++) {
				getTileBounds(tileX, tileY, tileBounds);
				if (tileBounds.isEmpty()) {
					stream.skipBytes(blockSize);
					continue;
				}
				
				getTile(raster, colorModel, x, y, Math.min(4, maxX - x), Math.min(4, maxY - y), uncompressed);
				
				Squish.compress(uncompressed, 0xFFFFFFFF, compressed, 0, compressionType, Squish.CompressionMethod.CLUSTER_FIT,
						Squish.CompressionMetric.UNIFORM, false);
				
				stream.write(compressed, 0, blockSize);
			}
		}
	}
	
	private void writeUncompressed(int width, int height) throws IOException
	{
		int[] bandOffsets = null;
		boolean bgrOrder = true;
		
		if (sampleModel instanceof ComponentSampleModel) {
			bandOffsets = ((ComponentSampleModel) sampleModel).getBandOffsets();
			if (sampleModel instanceof BandedSampleModel) {
				// for images with BandedSampleModel we can not work
				//  with raster directly and must use writePixels()
				bgrOrder = false;
			} else {
				// we can work with raster directly only in case of
				// BGR component order.
				// In any other case we must use writePixels()
				for (int i = 0; i < bandOffsets.length; i++) {
					bgrOrder &= (bandOffsets[i] == (bandOffsets.length - i - 1));
				}
			}
		} else {
			if (sampleModel instanceof SinglePixelPackedSampleModel) {
				
				// BugId 4892214: we can not work with raster directly
				// if image have different color order than RGB.
				// We should use writePixels() for such images.
				int[] bitOffsets = ((SinglePixelPackedSampleModel) sampleModel).getBitOffsets();
				for (int i = 0; i < bitOffsets.length - 1; i++) {
					bgrOrder &= bitOffsets[i] > bitOffsets[i + 1];
				}
			}
		}
		
		if (bandOffsets == null) {
			// we will use getPixels() to extract pixel data for writePixels()
			// Please note that getPixels() provides rgb bands order.
			bandOffsets = new int[numBands];
			for (int i = 0; i < numBands; i++) {
				bandOffsets[i] = i;
			}
		}
		
		int destScanlineBytes = width * numBands;
		
		int padding = destScanlineBytes % 4;
		if (padding != 0) {
			padding = 4 - padding;
		}
		int scanlineBytes = width * numBands;
		
		int[] pixels = new int[scanlineBytes * scaleX];
		
		int destScanlineLength = destScanlineBytes;
		
		int maxBandOffset = bandOffsets[0];
		for (int i = 1; i < bandOffsets.length; i++) {
			if (bandOffsets[i] > maxBandOffset) {
				maxBandOffset = bandOffsets[i];
			}
		}
		
		int[] pixel = new int[maxBandOffset + 1];
		if (noTransform && noSubband) {
			destScanlineLength = destScanlineBytes / (DataBuffer.getDataTypeSize(dataType) >> 3);
		}
		for (int i = 0; i < height; i++) {
			if (abortRequested()) {
				break;
			}
			
			int row = destinationRegion.y + i;
			
			// Get the pixels
			Raster src = inputRaster;
			
			Rectangle srcRect =
					new Rectangle(destinationRegion.x * scaleX + xOffset,
								row * scaleY + yOffset,
								(width - 1) * scaleX + 1,
								1);
			if (src == null) {
				src = input.getData(srcRect);
			}
			if (noTransform && noSubband) {
				SampleModel sm = src.getSampleModel();
				int pos = 0;
				int startX = srcRect.x - src.getSampleModelTranslateX();
				int startY = srcRect.y - src.getSampleModelTranslateY();
				if (sm instanceof ComponentSampleModel) {
					ComponentSampleModel csm = (ComponentSampleModel) sm;
					pos = csm.getOffset(startX, startY, 0);
					for (int nb = 1; nb < csm.getNumBands(); nb++) {
						if (pos > csm.getOffset(startX, startY, nb)) {
							pos = csm.getOffset(startX, startY, nb);
						}
					}
				} else if (sm instanceof MultiPixelPackedSampleModel) {
					MultiPixelPackedSampleModel mppsm =
							(MultiPixelPackedSampleModel) sm;
					pos = mppsm.getOffset(startX, startY);
				} else if (sm instanceof SinglePixelPackedSampleModel) {
					SinglePixelPackedSampleModel sppsm =
							(SinglePixelPackedSampleModel) sm;
					pos = sppsm.getOffset(startX, startY);
				}
				
				switch (dataType) {
				case DataBuffer.TYPE_BYTE:
					byte[] bdata =
								((DataBufferByte) src.getDataBuffer()).getData();
					stream.write(bdata, pos, destScanlineLength);
					break;
				
				case DataBuffer.TYPE_SHORT:
					short[] sdata =
								((DataBufferShort) src.getDataBuffer()).getData();
					stream.writeShorts(sdata, pos, destScanlineLength);
					break;
				
				case DataBuffer.TYPE_USHORT:
					short[] usdata =
								((DataBufferUShort) src.getDataBuffer()).getData();
					stream.writeShorts(usdata, pos, destScanlineLength);
					break;
				
				case DataBuffer.TYPE_INT:
					int[] idata =
								((DataBufferInt) src.getDataBuffer()).getData();
					stream.writeInts(idata, pos, destScanlineLength);
					break;
				}
				
				for (int k = 0; k < padding; k++) {
					stream.writeByte(0);
				}
			} else {
				src.getPixels(srcRect.x, srcRect.y,
						srcRect.width, srcRect.height, pixels);
				
				if ((scaleX != 1) || (maxBandOffset != numBands - 1)) {
					for (int j = 0, k = 0, n = 0; j < width; j++, k += scaleX * numBands, n += numBands) {
						System.arraycopy(pixels, k, pixel, 0, pixel.length);
					}
				}
//				writePixels(0, scanlineBytes, header.pfRGBBitCount, pixels,padding);
			}
		}
	}
	
	/*
	private void writePixels(int l, int scanlineBytes, int bitCount, int[] pixels, int padding)
	{
		int pixel = 0;
	    int k = 0;
	    switch (bitCount) {
	    case 8:
	       
	            for (int j=0; j<scanlineBytes; j++) {
	                bpixels[j] = (byte)pixels[l++];
	            }
	            stream.write(bpixels, 0, scanlineBytes);
	        
	        break;

	    case 16:
	        if (spixels == null) {
				spixels = new short[scanlineBytes / numBands];
			}
	        /*
	         * We expect that pixel data comes in RGB order.
	         * We will assemble short pixel taking into account
	         * the compression type:
	         *
	         * BI_RGB        - the RGB order should be maintained.
	         * BI_BITFIELDS  - use bitPos array that was built
	         *                 according to bitfields masks.
	         *
	        for (int j = 0, m = 0; j < scanlineBytes; m++) {
	            spixels[m] = 0;
	            if (compressionType == BMPConstants.BI_RGB) {
	                /*
	                 * please note that despite other cases,
	                 * the 16bpp BI_RGB requires the RGB data order
	                 *
	                spixels[m] = (short)
	                    (((0x1f & pixels[j    ]) << 10) |
	                     ((0x1f & pixels[j + 1]) <<  5) |
	                     ((0x1f & pixels[j + 2])      ));
	                 j += 3;
	            } else {
	                for(int i = 0 ; i < numBands; i++, j++) {
	                    spixels[m] |=
	                        (((pixels[j]) << bitPos[i]) & bitMasks[i]);
	                }
	            }
	        }
	        stream.writeShorts(spixels, 0, spixels.length);
	        break;

	    case 24:
	            for (int j=0; j<scanlineBytes; j+=3) {
	                // Since BMP needs BGR format
	                bpixels[k++] = (byte)(pixels[l+2]);
	                bpixels[k++] = (byte)(pixels[l+1]);
	                bpixels[k++] = (byte)(pixels[l]);
	                l+=3;
	            }
	            stream.write(bpixels, 0, scanlineBytes);
	        break;

	    case 32:
	        if (ipixels == null) {
				ipixels = new int[scanlineBytes / numBands];
			}
	        if (numBands == 3) {
	            /*
	             * We expect that pixel data comes in RGB order.
	             * We will assemble int pixel taking into account
	             * the compression type.
	             *
	             * BI_RGB        - the BGR order should be used.
	             * BI_BITFIELDS  - use bitPos array that was built
	             *                 according to bitfields masks.
	             *
	            for (int j = 0, m = 0; j < scanlineBytes; m++) {
	                ipixels[m] = 0;
	                    ipixels[m] =
	                        ((0xff & pixels[j + 2]) << 16) |
	                        ((0xff & pixels[j + 1]) <<  8) |
	                        ((0xff & pixels[j    ])      );
	                    j += 3;
	                
	            }
	        } else {
	            // We have two possibilities here:
	            // 1. we are writing the indexed image with bitfields
	            //    compression (this covers also the case of BYTE_BINARY)
	            //    => use icm to get actual RGB color values.
	            // 2. we are writing the gray-scaled image with BI_BITFIELDS
	            //    compression
	            //    => just replicate the level of gray to color components.
	            for (int j = 0; j < scanlineBytes; j++) {
	               
	                    ipixels[j] =
	                        pixels[j] << 16 | pixels[j] << 8 | pixels[j];
	                
	            }
	        }
	        stream.writeInts(ipixels, 0, ipixels.length);
	        break;
	    }
	    
	    for(k=0; k<padding; k++) {
	        stream.writeByte(0);
	    }
	}
	*/
	private void getTile(Raster raster, ColorModel colorModel, int startX, int startY, int w, int h, byte[] rgbArray)
	{
		int yoff = 0;
		int off;
		Object data;
		int nbands = raster.getNumBands();
		int dataType = raster.getDataBuffer().getDataType();
		switch (dataType) {
		case DataBuffer.TYPE_BYTE:
			data = new byte[nbands];
			break;
		case DataBuffer.TYPE_USHORT:
			data = new short[nbands];
			break;
		case DataBuffer.TYPE_INT:
			data = new int[nbands];
			break;
		case DataBuffer.TYPE_FLOAT:
			data = new float[nbands];
			break;
		case DataBuffer.TYPE_DOUBLE:
			data = new double[nbands];
			break;
		default:
			throw new IllegalArgumentException("Unknown data buffer type: " + dataType);
		}
		
		if (rgbArray == null) {
			rgbArray = new byte[h * 16];
		}
		
		for (int y = startY; y < startY + h; y++, yoff += 4) {
			off = yoff;
			for (int x = startX; x < startX + w; x++) {
				data = raster.getDataElements(x, y, data);
				rgbArray[off++] = (byte) (colorModel.getRed(data) & 0xFF);
				rgbArray[off++] = (byte) (colorModel.getGreen(data) & 0xFF);
				rgbArray[off++] = (byte) (colorModel.getBlue(data) & 0xFF);
				rgbArray[off++] = (byte) (colorModel.getAlpha(data) & 0xFF);
				
			}
		}
	}
	
	private void getTileBounds(int tileX, int tileY, Rectangle dest)
	{
		dest.x = tileX * 4;
		dest.y = tileY * 4;
		
		dest.width = 4;
		dest.height = 4;
		
		//		int wk=(dest.x+4)-width;//width correction
		//		if(wk>0){
		//			dest.width-=wk;
		//		}
		//
		//		int hk=(dest.y+4)-height;//height correction
		//		if(hk>0){
		//			dest.height-=hk;
		//		}
		
		Rectangle.intersect(dest, destinationRegion, dest);
	}
	
	@SuppressWarnings("unused")
	private void getTile(Raster raster, ColorModel colorModel, int startX, int startY, int w, int h, int[] rgbArray)
	{
		int yoff = 0;
		int off;
		Object data;
		int nbands = raster.getNumBands();
		int dataType = raster.getDataBuffer().getDataType();
		switch (dataType) {
		case DataBuffer.TYPE_BYTE:
			data = new byte[nbands];
			break;
		case DataBuffer.TYPE_USHORT:
			data = new short[nbands];
			break;
		case DataBuffer.TYPE_INT:
			data = new int[nbands];
			break;
		case DataBuffer.TYPE_FLOAT:
			data = new float[nbands];
			break;
		case DataBuffer.TYPE_DOUBLE:
			data = new double[nbands];
			break;
		default:
			throw new IllegalArgumentException("Unknown data buffer type: " + dataType);
		}
		
		if (rgbArray == null) {
			rgbArray = new int[h * 4];
		}
		
		for (int y = startY; y < startY + h; y++, yoff += 4) {
			off = yoff;
			for (int x = startX; x < startX + w; x++) {
				rgbArray[off++] = colorModel.getRGB(raster.getDataElements(x, y, data));
			}
		}
	}
	
}
