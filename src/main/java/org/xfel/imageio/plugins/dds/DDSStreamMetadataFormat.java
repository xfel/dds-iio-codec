/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadataFormat;
import javax.imageio.metadata.IIOMetadataFormatImpl;

public class DDSStreamMetadataFormat extends IIOMetadataFormatImpl
{
	
	private static DDSStreamMetadataFormat instance;
	
	public static IIOMetadataFormat getInstance()
	{
		if (instance == null) {
			instance = new DDSStreamMetadataFormat();
		}
		return instance;
	}
	
	private DDSStreamMetadataFormat()
	{
		this(DDSConstants.nativeStreamMetadataFormatName);
	}
	
	DDSStreamMetadataFormat(String rootName)
	{
		super(rootName, CHILD_POLICY_SOME);
		
		addElement("General", rootName, CHILD_POLICY_SOME);
		
		addAttribute("General", "Width", DATATYPE_INTEGER, true, null, "0", "65535", true, true);
		addAttribute("General", "Height", DATATYPE_INTEGER, true, null, "0", "65535", true, true);
		addAttribute("General", "Depth", DATATYPE_INTEGER, false, null, "0", "65535", true, true);
		addAttribute("General", "Mipmaps", DATATYPE_INTEGER, false, null, "1", "65535", true, true);
		
		addElement("Cubemap", "General", CHILD_POLICY_SOME);
		
		addElement("PositiveX", "Cubemap", CHILD_POLICY_EMPTY);
		addElement("NegativeX", "Cubemap", CHILD_POLICY_EMPTY);
		addElement("PositiveY", "Cubemap", CHILD_POLICY_EMPTY);
		addElement("NegativeY", "Cubemap", CHILD_POLICY_EMPTY);
		addElement("PositiveZ", "Cubemap", CHILD_POLICY_EMPTY);
		addElement("NegativeZ", "Cubemap", CHILD_POLICY_EMPTY);
		
		addElement("PixelFormat", rootName, CHILD_POLICY_CHOICE);
		
		addAttribute("PixelFormat", "PixelSize", DATATYPE_INTEGER, true, "32", "8", "32", true, true);
		
		addElement("RGB", "PixelFormat", CHILD_POLICY_EMPTY);
		
		addAttribute("RGB", "Red", DATATYPE_INTEGER, true, "255", "0", "255", true, true);
		addAttribute("RGB", "Green", DATATYPE_INTEGER, true, "255", "0", "255", true, true);
		addAttribute("RGB", "Blue", DATATYPE_INTEGER, true, "255", "0", "255", true, true);
		addAttribute("RGB", "Alpha", DATATYPE_INTEGER, false, "255", "0", "255", true, true);
		
		addElement("FourCC", "PixelFormat", CHILD_POLICY_EMPTY);
		addAttribute("FourCC", "Value", DATATYPE_STRING, true, null);
		
		addElement("DXGI_FORMAT", "PixelFormat", CHILD_POLICY_EMPTY);
		addAttribute("DXGI_FORMAT", "Value", DATATYPE_STRING, true, null);
	}
	
	@Override
	public boolean canNodeAppear(String elementName, ImageTypeSpecifier imageType)
	{
		return true;
	}
	
}
