/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import javax.imageio.metadata.IIOInvalidTreeException;

import org.w3c.dom.Node;

public class DDSImageMetadata extends DDSMetadata
{
	
	public DDSImageMetadata()
	{
		super(false);
	}
	
	@Override
	protected void mergeNativeTree(Node root) throws IIOInvalidTreeException
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void mergeStandardTree(Node root) throws IIOInvalidTreeException
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected Node getNativeTree()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isReadOnly()
	{
		return true;
	}
	
	@Override
	public void reset()
	{
		// TODO Auto-generated method stub
		
	}
	
}
