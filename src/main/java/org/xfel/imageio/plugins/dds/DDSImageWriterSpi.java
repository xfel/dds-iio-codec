/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.io.IOException;
import java.util.Locale;

import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriter;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;

public class DDSImageWriterSpi extends ImageWriterSpi
{
	
	public DDSImageWriterSpi()
	{
		super(DDSConstants.vendorName, DDSConstants.version, DDSConstants.names, DDSConstants.suffixes, DDSConstants.MIMETypes, DDSConstants.writerClassName,
				new Class[]{ImageOutputStream.class}, DDSConstants.readerSpiNames, DDSConstants.supportsStandardStreamMetadataFormat,
				DDSConstants.nativeStreamMetadataFormatName, DDSConstants.nativeStreamMetadataFormatClassName, null, null,
				DDSConstants.supportsStandardImageMetadataFormat, DDSConstants.nativeImageMetadataFormatName, DDSConstants.nativeImageMetadataFormatClassName,
				null, null);
	}
	
	@Override
	public boolean canEncodeImage(ImageTypeSpecifier type)
	{
		ColorModel cm=type.getColorModel();
		if (cm instanceof IndexColorModel)
			return false;
		
		return true;
	}
	
	@Override
	public ImageWriter createWriterInstance(Object extension) throws IOException
	{
		return new DDSImageWriter(this);
	}
	
	@Override
	public String getDescription(Locale locale)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
