/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import java.io.IOException;
import java.util.Locale;

import javax.imageio.ImageReader;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

public class DDSImageReaderSpi extends ImageReaderSpi
{
	
	public DDSImageReaderSpi()
	{
		super(DDSConstants.vendorName, DDSConstants.version, DDSConstants.names, DDSConstants.suffixes, DDSConstants.MIMETypes, DDSConstants.readerClassName,
				new Class[]{ImageInputStream.class}, DDSConstants.writerSpiNames, DDSConstants.supportsStandardStreamMetadataFormat,
				DDSConstants.nativeStreamMetadataFormatName, DDSConstants.nativeStreamMetadataFormatClassName, null, null,
				DDSConstants.supportsStandardImageMetadataFormat, DDSConstants.nativeImageMetadataFormatName, DDSConstants.nativeImageMetadataFormatClassName,
				null, null);
	}
	
	@Override
	public boolean canDecodeInput(Object input) throws IOException
	{
		if (!(input instanceof ImageInputStream)) {
			return false;
		}
		
		ImageInputStream stream = (ImageInputStream) input;
		
		byte[] b = new byte[4];
		
		stream.mark();
		stream.readFully(b, 0, 4);
		stream.reset();
		
		return (b[0] == 'D') && (b[1] == 'D') && (b[2] == 'S') && (b[3] == ' ');
	}
	
	@Override
	public ImageReader createReaderInstance(Object unused) throws IOException
	{
		return new DDSImageReader(this);
	}
	
	@Override
	public String getDescription(Locale locale)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
}
