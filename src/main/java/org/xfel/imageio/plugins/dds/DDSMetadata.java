/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataFormatImpl;

import org.w3c.dom.Node;

abstract class DDSMetadata extends IIOMetadata implements DDSConstants
{
	
	public final boolean isStream;
	
	protected DDSMetadata(boolean isStream)
	{
		if (isStream) {
			standardFormatSupported = supportsStandardStreamMetadataFormat;
			nativeMetadataFormatName = nativeStreamMetadataFormatName;
			nativeMetadataFormatClassName = nativeStreamMetadataFormatClassName;
		} else {
			standardFormatSupported = supportsStandardImageMetadataFormat;
			nativeMetadataFormatName = nativeImageMetadataFormatName;
			nativeMetadataFormatClassName = nativeImageMetadataFormatClassName;
		}
		this.isStream = isStream;
	}
	
	protected abstract void mergeNativeTree(Node root)
			throws IIOInvalidTreeException;
	
	protected abstract void mergeStandardTree(Node root)
			throws IIOInvalidTreeException;
	
	protected abstract Node getNativeTree();
	
	@Override
	public Node getAsTree(String formatName)
	{
		if (formatName.equals(nativeMetadataFormatName)) {
			return getNativeTree();
		} else if (formatName.equals
					(IIOMetadataFormatImpl.standardMetadataFormatName)) {
			return getStandardTree();
		} else {
			throw new IllegalArgumentException("Not a recognized format!");
		}
	}
	
	@Override
	public void mergeTree(String formatName, Node root) throws IIOInvalidTreeException
	{
		if (formatName.equals(nativeMetadataFormatName)) {
			if (root == null) {
				throw new IllegalArgumentException("root == null!");
			}
			mergeNativeTree(root);
		} else if (formatName.equals
					(IIOMetadataFormatImpl.standardMetadataFormatName)) {
			if (root == null) {
				throw new IllegalArgumentException("root == null!");
			}
			mergeStandardTree(root);
		} else {
			throw new IllegalArgumentException("Not a recognized format!");
		}
	}
	
}
