/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xfel.imageio.plugins.dds;

import java.util.Arrays;

import javax.imageio.metadata.IIOMetadataFormat;

public class DDSImageMetadataFormat extends DDSStreamMetadataFormat
{
	
	private static DDSImageMetadataFormat instance;
	
	public static IIOMetadataFormat getInstance()
	{
		if (instance == null) {
			instance = new DDSImageMetadataFormat();
		}
		return instance;
	}
	
	private DDSImageMetadataFormat()
	{
		super(DDSConstants.nativeImageMetadataFormatName);
		
		addElement("Image", DDSConstants.nativeImageMetadataFormatName, CHILD_POLICY_EMPTY);
		
		addAttribute("Image", "Mipmap", DATATYPE_INTEGER, false, null, "0", "65535", true, true);
		addAttribute("Image", "DepthLevel", DATATYPE_INTEGER, false, null, "0", "65535", true, true);
		addAttribute("Image", "CubemapFace", DATATYPE_STRING, false, null,
				Arrays.asList("PositiveX", "NegativeX", "PositiveY", "NegativeY", "PositiveZ", "NegativeZ"));
	}
	
}
