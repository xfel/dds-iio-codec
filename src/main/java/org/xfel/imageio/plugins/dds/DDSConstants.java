/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.xfel.imageio.plugins.dds;

public interface DDSConstants {

	// values for DDS_HEADER.dwFlags
	/** Required in every .dds file. */
	int DDSD_CAPS = 0x1;
	/** Required in every .dds file. */
	int DDSD_HEIGHT = 0x2;
	/** Required in every .dds file. */
	int DDSD_WIDTH = 0x4;
	/** Required in every .dds file. */
	int DDSD_PIXELFORMAT = 0x1000;
	/** Summary of all required DDSD flags. */
	int DDSD_REQUIRED = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT;
	/** Required when pitch is provided for an uncompressed texture. */
	int DDSD_PITCH = 0x8;
	/** Required in a mipmapped texture. */
	int DDSD_MIPMAPCOUNT = 0x20000;
	/** Required when pitch is provided for a compressed texture. */
	int DDSD_LINEARSIZE = 0x80000;
	/** Required in a depth texture. */
	int DDSD_DEPTH = 0x800000;

	// values for DDS_PIXELFORMAT.dwFlags
	/** Texture contains alpha data; dwRGBAlphaBitMask contains valid data. */
	int DDPF_ALPHAPIXELS = 0x1;
	/**
	 * Used in some older DDS files for alpha channel only uncompressed data
	 * (dwRGBBitCount contains the alpha channel bitcount; dwABitMask contains
	 * valid data)
	 */
	int DDPF_ALPHA = 0x2;
	/** Texture contains compressed RGB data; dwFourCC contains valid data. */
	int DDPF_FOURCC = 0x4;
	/**
	 * Texture contains uncompressed RGB data; dwRGBBitCount and the RGB masks
	 * (dwRBitMask, dwRBitMask, dwRBitMask) contain valid data.
	 */
	int DDPF_RGB = 0x40;
	/**
	 * Used in some older DDS files for YUV uncompressed data (dwRGBBitCount
	 * contains the YUV bit count; dwRBitMask contains the Y mask, dwGBitMask
	 * contains the U mask, dwBBitMask contains the V mask)
	 */
	int DDPF_YUV = 0x200;
	/**
	 * Used in some older DDS files for single channel color uncompressed data
	 * (dwRGBBitCount contains the luminance channel bit count; dwRBitMask
	 * contains the channel mask). Can be combined with DDPF_ALPHAPIXELS for a
	 * two channel DDS file.
	 */
	int DDPF_LUMINANCE = 0x20000;

	// values for DDS_HEADER.dwSurfaceFlags
	/**
	 * Optional; must be used on any file that contains more than one surface (a
	 * mipmap, a cubic environment map, or mipmapped volume texture).
	 */
	int DDSCAPS_COMPLEX = 0x8;
	/** Optional; should be used for a mipmap. */
	int DDSCAPS_MIPMAP = 0x400000;
	/** Required */
	int DDSCAPS_TEXTURE = 0x1000;

	// values for DDS_HEADER.dwCubemapFlags
	/** Required for a cube map. */
	int DDSCAPS2_CUBEMAP = 0x200;
	/** Required when these surfaces are stored in a cube map. */
	int DDSCAPS2_CUBEMAP_POSITIVEX = 0x400;
	/** Required when these surfaces are stored in a cube map. */
	int DDSCAPS2_CUBEMAP_NEGATIVEX = 0x800;
	/** Required when these surfaces are stored in a cube map. */
	int DDSCAPS2_CUBEMAP_POSITIVEY = 0x1000;
	/** Required when these surfaces are stored in a cube map. */
	int DDSCAPS2_CUBEMAP_NEGATIVEY = 0x2000;
	/** Required when these surfaces are stored in a cube map. */
	int DDSCAPS2_CUBEMAP_POSITIVEZ = 0x4000;
	/** Required when these surfaces are stored in a cube map. */
	int DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x8000;
	/** Required for a volume texture. */
	int DDSCAPS2_VOLUME = 0x200000;

	// fourCC codes
	int D3DFMT_DXT1 = 0x31545844;
	int D3DFMT_DXT2 = 0x32545844;
	int D3DFMT_DXT3 = 0x33545844;
	int D3DFMT_DXT4 = 0x34545844;
	int D3DFMT_DXT5 = 0x35545844;

	int D3DFMT_A16B16G16R16 = 36;
	int D3DFMT_Q16W16V16U16 = 110;
	int D3DFMT_R16F = 111;
	int D3DFMT_G16R16F = 112;
	int D3DFMT_A16B16G16R16F = 113;
	int D3DFMT_R32F = 114;
	int D3DFMT_G32R32F = 115;
	int D3DFMT_A32B32G32R32F = 116;

	int FOURCC_DX10 = 0x30315844;

	// spi values
	String vendorName = "Felix Treede";
	String version = "1.0";
	String[] names = { "dds", "DDS", "DirectDraw Surface" };
	String[] suffixes = { "dds" };
	String[] MIMETypes = { "image/dds" };

	String readerClassName = "org.xfel.imageio.plugins.dds.DDSImageReader";
	String[] readerSpiNames = { "org.xfel.imageio.plugins.dds.DDSImageReaderSpi" };

	String writerClassName = "org.xfel.imageio.plugins.dds.DDSImageWriter";
	String[] writerSpiNames = { "org.xfel.imageio.plugins.dds.DDSImageWriterSpi" };

	boolean supportsStandardStreamMetadataFormat = false;
	String nativeStreamMetadataFormatName = "xfel-imageio_dds_stream_1.0";
	String nativeStreamMetadataFormatClassName = "org.xfel.imageio.plugins.dds.DDSStreamMetadataFormat";

	boolean supportsStandardImageMetadataFormat = false;
	String nativeImageMetadataFormatName = "xfel-imageio_dds_image_1.0";
	String nativeImageMetadataFormatClassName = "org.xfel.imageio.plugins.dds.DDSImageMetadataFormat";

}
