/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JComponent;
import javax.swing.JFrame;

import org.apache.commons.io.input.CountingInputStream;
import org.xfel.imageio.plugins.dds.DDSImageReaderSpi;

@SuppressWarnings("serial")
public class ReaderTest extends JComponent
{
	
	public static void main(String[] args) throws Exception
	{
//		StringBuffer sb = new StringBuffer();
//		for (int i = 1; i < args.length; i++) {
//			sb.append(args[i]);
//			sb.append(' ');
//		}
//		File input = new File(sb.toString().trim());
		File input = new File("D:\\EEP-Ressourcen\\EEP 8\\Gleisstile\\Gleise\\Bhst900_Kreihnsdoerp.3dm");
		
//		int mipmap = Integer.parseInt(args[0]);
		int mipmap=0;
		
		System.out.println(input);
		Image img;//=ImageIO.read(input);
		//if (img == null) {
		//
		
		Dimension size = null;
		
		FileInputStream fis=new FileInputStream(input);
		fis.skip(0x00019d91);
		
		CountingInputStream cin=new CountingInputStream(fis);
		
		ImageInputStream stream = ImageIO.createImageInputStream(cin);
		ImageReader reader = null;
		try {
			Iterator<?> iter = ImageIO.getImageReaders(stream);
			if (iter.hasNext()) {
				reader = (ImageReader) iter.next();
			} else {
				System.err.println("reader not found");
				reader = new DDSImageReaderSpi().createReaderInstance();
			}
			
			reader.setInput(stream, true, true);
			ImageReadParam param = reader.getDefaultReadParam();
			
			param.setSourceSubsampling(2, 2, 0, 0);
			
			if (mipmap == 0) {
				size = new Dimension(reader.getWidth(0), reader.getHeight(0));
				img = reader.read(0, param);
			} else {
				size = new Dimension(reader.getThumbnailWidth(0, mipmap - 1), reader.getThumbnailHeight(0, mipmap - 1));
				img = reader.readThumbnail(0, mipmap - 1);
			}
		} finally {
			if (reader != null) {
				reader.dispose();
			}
			stream.close();
		}
		//}
		if (img == null) {
			System.err.println("no image");
			return;
		}
		JFrame f = new JFrame();
		f.setResizable(false);
		ReaderTest comp = new ReaderTest(img);
		comp.setPreferredSize(size);
		f.add(comp);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.pack();
		f.setVisible(true);
		
		System.out.println(cin.getByteCount());
	}
	
	private Image img;
	
	public ReaderTest(Image img)
	{
		super();
		this.img = img;
		setPreferredSize(new Dimension(img.getWidth(this), img.getHeight(this)));
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		g.drawImage(img, 0, 0, Color.CYAN, this);
	}
}
