/*
 * This file is part of DDS ImageIO Plugin.
 * 
 * DDS ImageIO Plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DDS ImageIO Plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DDS ImageIO Plugin.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.JFrame;

import org.xfel.imageio.plugins.dds.DDSImageReaderSpi;
import org.xfel.imageio.plugins.dds.DDSImageWriteParam;


public class WriterTest
{
	public static void main(String[] args) throws Exception
	{
		int mipmap=Integer.parseInt(args[0]);
		File input = new File(args[1]);
		
		File tmp=File.createTempFile(input.getName(), ".dds");
		
		ImageInputStream in = null;
		ImageOutputStream out = null;
		
		ImageReader reader = null;
		ImageWriter writer = null;
		
		try {
			in = ImageIO.createImageInputStream(input);
			out = ImageIO.createImageOutputStream(tmp);
			
			Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
			if (!readers.hasNext())
				return;
			reader = readers.next();
			reader.setInput(in, true, false);
			
			Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("dds");
			if (!writers.hasNext())
				return;
			writer = writers.next();
			writer.setOutput(out);
			
//			writer.convertImageMetadata(reader.getImageMetadata(reader.getMinIndex()), imageType, param)
			IIOMetadata metadata = reader.getImageMetadata(reader.getMinIndex());
			BufferedImage img = reader.read(reader.getMinIndex());
			
			ImageTypeSpecifier type = new ImageTypeSpecifier(img);
			
			DDSImageWriteParam param = new DDSImageWriteParam();
			
			ArrayList<BufferedImage> mipMaps = new ArrayList<BufferedImage>();
			
			int w = img.getWidth();
			int h = img.getHeight();
			
//			while (true) {
//				w = Math.max(w >> 1, 1);
//				h = Math.max(h >> 1, 1);
//				
//				BufferedImage map = type.createBufferedImage(w, h);
//				
//				Graphics2D g = map.createGraphics();
//				try {
//					g.drawImage(img, 0, 0, w, h, null);
//				} finally {
//					g.dispose();
//				}
//				
//				mipMaps.add(map);
//				
//				if ((w == 1) && (h == 1)) {
//					break;
//				}
//			}
			
			writer.write(null, new IIOImage(img, mipMaps, metadata), param);
		} finally {
			if (reader != null) {
				reader.dispose();
			}
			if (writer != null) {
				writer.dispose();
			}
			
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {}
			}
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {}
			}
		}
		
		Image img=null;
		Dimension size=null;
		
		ImageInputStream stream = ImageIO.createImageInputStream(tmp);
		reader = null;
		try {
			Iterator<?> iter = ImageIO.getImageReaders(stream);
			if (iter.hasNext()) {
				reader = (ImageReader) iter.next();
			} else {
				System.err.println("reader not found");
				reader = new DDSImageReaderSpi().createReaderInstance();
			}
			
			reader.setInput(stream, true, true);
			ImageReadParam param = reader.getDefaultReadParam();
			
			param.setSourceSubsampling(2, 2, 0, 0);
			
			if (mipmap == 0) {
				size = new Dimension(reader.getWidth(0), reader.getHeight(0));
				img = reader.read(0, param);
			} else {
				size = new Dimension(reader.getThumbnailWidth(0, mipmap - 1), reader.getThumbnailHeight(0, mipmap - 1));
				img = reader.readThumbnail(0, mipmap - 1);
			}
		} finally {
			if (reader != null) {
				reader.dispose();
			}
			stream.close();
		}
		//}
		if (img == null) {
			System.err.println("no image");
			return;
		}
		JFrame f = new JFrame();
		f.setResizable(false);
		ReaderTest comp = new ReaderTest(img);
		comp.setPreferredSize(size);
		f.add(comp);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.pack();
		f.setVisible(true);
	}
}
